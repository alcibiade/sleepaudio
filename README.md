
# Sleepaudio: An environmental music generation Framework

Sleep Audio is a set of components organized around a Java algorithmic composition framework. 
Its aim is to provide a way to generate Environmental music. It currently 
exports to CSound's for its high-quality rendering abilities, but also tries to provide a built-in synthesis system. 
