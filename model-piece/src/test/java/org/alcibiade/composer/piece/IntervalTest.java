package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class IntervalTest {

    /*
     * Test method for 'org.alcibiade.composer.Interval.Interval(int)'
     */
    @Test
    public void testInterval() {
        Interval it = new Interval(4);
        assertEquals(4, it.getHalfTones());
        assertEquals(it, Interval.builder().halfTones(4).build());
        assertNotEquals(it, Interval.builder().halfTones(5).build());
    }

    /*
     * Test method for 'org.alcibiade.composer.Interval.getHalfTones()'
     */
    @Test
    public void testGetHalfTones() {
        Interval it = new Interval(4);
        assertEquals(4, it.getHalfTones());
    }

    @Test
    public void testAdd() {
        Interval it = new Interval(4);
        Interval i2 = it.add(Interval.OCTAVE);
        assertEquals(4, it.getHalfTones());
        assertEquals(16, i2.getHalfTones());
    }

    @Test
    public void testSubtract() {
        Interval it = new Interval(4);
        Interval i2 = it.subtract(Interval.OCTAVE);
        assertEquals(4, it.getHalfTones());
        assertEquals(-8, i2.getHalfTones());
    }

    @Test
    public void testAddHalfTones() {
        Interval it = new Interval(4);
        Interval i2 = it.addHalfTones(6);
        assertEquals(4, it.getHalfTones());
        assertEquals(10, i2.getHalfTones());
    }

    @Test
    public void testSubtractHalfTones() {
        Interval it = new Interval(4);
        Interval i2 = it.subtractHalfTones(6);
        assertEquals(4, it.getHalfTones());
        assertEquals(-2, i2.getHalfTones());
    }
}
