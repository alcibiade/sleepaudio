package org.alcibiade.composer.piece;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class KeyTest {

    @Test
    public void testKey() {
        new Key(Pitch.C2, Scale.MINOR);
        new Key(Pitch.E4, Scale.MAJOR);
    }

    @Test
    public void testGetTonic() {
        Key key = new Key(Pitch.E4, Scale.MAJOR);
        assertEquals(Pitch.E4, key.getTonic());
    }

    @Test
    public void testGetScale() {
        Key key = new Key(Pitch.E4, Scale.MAJOR);
        assertEquals(Scale.MAJOR, key.getScale());
    }

    @Test
    public void testContains() {
        Key key = new Key(Pitch.C4, Scale.MINOR);
        Assertions.assertThat(key.contains(Pitch.A4)).isFalse();
        Assertions.assertThat(key.contains(Pitch.G2)).isTrue();
        Assertions.assertThat(key.contains(Pitch.C3)).isTrue();
        Assertions.assertThat(key.contains(Pitch.G4)).isTrue();
        Assertions.assertThat(key.contains(Pitch.E3)).isFalse();
        Assertions.assertThat(key.contains(Pitch.E5)).isFalse();
    }

    @Test
    public void testToString() {
        Key key = new Key(Pitch.C4, Scale.MINOR);
        assertNotNull(key.toString());
    }
}
