package org.alcibiade.composer.piece;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TrackTest {

    private static final Note n1 = new Note(Pitch.C3, new Clock(2), Velocity.FF);
    private static final Note n2 = new Note(Pitch.E3, new Clock(2), Velocity.FF);
    private static final Note n3 = new Note(Pitch.G3, new Clock(2), Velocity.FF);
    private Track<Event<Note>> sampleTrack;

    @Before
    public void setUp() {
        sampleTrack = new Track<>("Test track",
                Instrument.GLOCKENSPIEL);
        sampleTrack.addEvent(new Event<>(new Clock(1.), n1));
        sampleTrack.addEvent(new Event<>(new Clock(1.), n3));
        sampleTrack.addEvent(new Event<>(new Clock(2.), n2));
        sampleTrack.addEvent(new Event<>(new Clock(3.), n3));
    }

    @Test
    public void testTrack() {
        new Track<>("Test track", Instrument.GLOCKENSPIEL);
    }

    @Test
    public void testGetEvents() {
        assertEquals(4, sampleTrack.getEvents().size());
    }

    @Test
    public void testGetEventsDouble() {
        assertEquals(0, sampleTrack.getEvents(new Clock(0.0)).size());
        assertEquals(0, sampleTrack.getEvents(new Clock(0.5)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(1.0)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(1.5)).size());
        assertEquals(3, sampleTrack.getEvents(new Clock(2.0)).size());
        assertEquals(3, sampleTrack.getEvents(new Clock(2.5)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.0)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.5)).size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.0)).size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.5)).size());
        assertEquals(0, sampleTrack.getEvents(new Clock(5.0)).size());
    }

    @Test
    public void testGetEventsDoubleDouble() {
        assertEquals(0, sampleTrack.getEvents(new Clock(0.0), new Clock(0.5)).size());
        assertEquals(0, sampleTrack.getEvents(new Clock(0.5), new Clock(1.0)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(0.5), new Clock(1.5)).size());
        assertEquals(3, sampleTrack.getEvents(new Clock(1.5), new Clock(2.5)).size());
        assertEquals(4, sampleTrack.getEvents(new Clock(2.5), new Clock(3.5)).size());
        assertEquals(4, sampleTrack.getEvents(new Clock(1.5), new Clock(3.5)).size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.5), new Clock(4.5)).size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.0), new Clock(5.5)).size());
        assertEquals(0, sampleTrack.getEvents(new Clock(5.0), new Clock(5.5)).size());
    }

    @Test
    public void testGetPitchesDouble() {
        assertEquals(0, sampleTrack.getEvents(new Clock(0.0)).getPitches().size());
        assertEquals(0, sampleTrack.getEvents(new Clock(0.5)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(1.0)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(1.5)).getPitches().size());
        assertEquals(3, sampleTrack.getEvents(new Clock(2.0)).getPitches().size());
        assertEquals(3, sampleTrack.getEvents(new Clock(2.5)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.0)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.5)).getPitches().size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.0)).getPitches().size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.5)).getPitches().size());
        assertEquals(0, sampleTrack.getEvents(new Clock(5.0)).getPitches().size());
    }

    @Test
    public void testGetPitchesDoubleDouble() {
        assertEquals(0, sampleTrack.getEvents(new Clock(0.0), new Clock(0.5)).getPitches().size());
        assertEquals(0, sampleTrack.getEvents(new Clock(0.5), new Clock(1.0)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(0.5), new Clock(1.5)).getPitches().size());
        assertEquals(3, sampleTrack.getEvents(new Clock(1.5), new Clock(2.5)).getPitches().size());
        assertEquals(3, sampleTrack.getEvents(new Clock(2.5), new Clock(3.5)).getPitches().size());
        assertEquals(3, sampleTrack.getEvents(new Clock(1.5), new Clock(3.5)).getPitches().size());
        assertEquals(2, sampleTrack.getEvents(new Clock(3.5), new Clock(4.5)).getPitches().size());
        assertEquals(1, sampleTrack.getEvents(new Clock(4.0), new Clock(5.5)).getPitches().size());
        assertEquals(0, sampleTrack.getEvents(new Clock(5.0), new Clock(5.5)).getPitches().size());
    }

    @Test
    public void testAddEvent() {
        Track<Event<Note>> track = new Track<>("Test track",
                Instrument.GLOCKENSPIEL);
        Event<Note> noteEvent = new Event<>(new Clock(2.), n2);

        assertEquals(0, track.getEvents().size());

        track.addEvent(noteEvent);
        assertEquals(1, track.getEvents().size());

        assertEquals(noteEvent, track.getEvents().iterator().next());
    }

    @Test
    public void testRemoveEvent() {
        Track<Event<Note>> track = new Track<>("Test track",
                Instrument.GLOCKENSPIEL);
        Event<Note> noteEvent = new Event<>(new Clock(2.), n2);
        track.addEvent(noteEvent);
        assertEquals(1, track.getEvents().size());
        track.removeEvent(noteEvent);
        assertEquals(0, track.getEvents().size());
    }

    @Test
    public void testGetInstrument() {
        Track<?> track = new Track<>("Test track",
                Instrument.GLOCKENSPIEL);
        assertEquals(Instrument.GLOCKENSPIEL, track.getInstrument());
    }

    @Test
    public void testGetName() {
        Track<?> track = new Track<>("Test track",
                Instrument.GLOCKENSPIEL);
        assertEquals("Test track", track.getName());
    }
}
