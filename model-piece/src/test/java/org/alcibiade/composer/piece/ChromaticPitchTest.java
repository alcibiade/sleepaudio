package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChromaticPitchTest {

    @Test
    public void testChromaticInformations() {
        ChromaticPitch a4 = Pitch.A4.getChromaticInformations();
        assertEquals('A', a4.getNoteLetter());
        assertEquals(4, a4.getOctave());
        assertEquals(false, a4.isSharp());

        ChromaticPitch b5 = Pitch.A4.add(Interval.OCTAVE.add(Interval.TONE)).getChromaticInformations();
        assertEquals('B', b5.getNoteLetter());
        assertEquals(5, b5.getOctave());
        assertEquals(false, b5.isSharp());

        ChromaticPitch fs3 = Pitch.A4.subtract(
                Interval.OCTAVE.add(Interval.TONE).add(Interval.HALF_TONE)).getChromaticInformations();
        assertEquals('F', fs3.getNoteLetter());
        assertEquals(3, fs3.getOctave());
        assertEquals(true, fs3.isSharp());
    }

    @Test
    public void testChromaticInformationsRoundUp() {
        ChromaticPitch a4 = new Pitch(Pitch.A4.getFrequency() + Pitch.EPSILON
                * 2.).getChromaticInformations();
        assertEquals('A', a4.getNoteLetter());
        assertEquals(4, a4.getOctave());
        assertEquals(false, a4.isSharp());

        ChromaticPitch b5 = new Pitch(Pitch.A4.getFrequency() + Pitch.EPSILON
                * 2.).add(Interval.OCTAVE.add(Interval.TONE)).getChromaticInformations();
        assertEquals('B', b5.getNoteLetter());
        assertEquals(5, b5.getOctave());
        assertEquals(false, b5.isSharp());

        ChromaticPitch fs3 = new Pitch(Pitch.A4.getFrequency() + Pitch.EPSILON
                * 2.).subtract(
                Interval.OCTAVE.add(Interval.TONE).add(Interval.HALF_TONE)).getChromaticInformations();
        assertEquals('F', fs3.getNoteLetter());
        assertEquals(3, fs3.getOctave());
        assertEquals(true, fs3.isSharp());
    }

    @Test
    public void testChromaticInformationsRoundDown() {
        ChromaticPitch a4 = new Pitch(Pitch.A4.getFrequency() - Pitch.EPSILON
                * 2.).getChromaticInformations();
        assertEquals('A', a4.getNoteLetter());
        assertEquals(4, a4.getOctave());
        assertEquals(false, a4.isSharp());

        ChromaticPitch b5 = new Pitch(Pitch.A4.getFrequency() - Pitch.EPSILON
                * 2.).add(Interval.OCTAVE.add(Interval.TONE)).getChromaticInformations();
        assertEquals('B', b5.getNoteLetter());
        assertEquals(5, b5.getOctave());
        assertEquals(false, b5.isSharp());

        ChromaticPitch fs3 = new Pitch(Pitch.A4.getFrequency() - Pitch.EPSILON
                * 2.).subtract(
                Interval.OCTAVE.add(Interval.TONE).add(Interval.HALF_TONE)).getChromaticInformations();
        assertEquals('F', fs3.getNoteLetter());
        assertEquals(3, fs3.getOctave());
        assertEquals(true, fs3.isSharp());
    }
}
