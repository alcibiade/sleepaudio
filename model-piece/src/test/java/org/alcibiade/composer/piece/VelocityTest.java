package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.*;

public class VelocityTest {

    @Test
    public void testHashCodeAndEquals() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Velocity v1 = new Velocity(i);
                Velocity v2 = new Velocity(j);

                assertEquals(i == j, v1.equals(v2));
                assertTrue(i != j || v1.hashCode() == v2.hashCode());
            }
        }
    }

    @Test
    public void testVelocity() {
        new Velocity(12);
        new Velocity(12, 36);
    }

    @Test
    public void testToString() {
        String s = Velocity.MF.toString();
        assertNotNull(s);
        assertTrue(s.length() > 0);
    }

    @Test
    public void testCompareTo() {
        assertTrue(Velocity.MP.compareTo(Velocity.FF) < 0);
        assertTrue(Velocity.FF.compareTo(Velocity.MP) > 0);
        assertTrue(Velocity.P.compareTo(Velocity.P) == 0);
    }

    @Test
    public void testToDouble() {
        Velocity v1 = new Velocity(Velocity.MAX_VALUE / 3,
                Velocity.MAX_VALUE * 2 / 3);
        assertEquals(1. / 3., v1.getDynamic(1.), 0.0001);
        assertEquals(2. / 3., v1.getBalance(1.), 0.0001);

        assertEquals(0.5, Velocity.FF.getBalance(1.0), 0.0001);
        assertEquals(1.0, Velocity.FF.getBalance(2.0), 0.0001);
    }
}
