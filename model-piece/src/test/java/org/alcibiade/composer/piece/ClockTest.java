package org.alcibiade.composer.piece;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClockTest {

    public static final double EPSILON = 1.0E-5;

    @Test
    public void testNew() {
        assertEquals(new Clock(), new Clock(0));
        assertEquals(new Clock(), new Clock(0, 1));

        try {
            new Clock(1, 0);
            assertTrue(false);
        } catch (ArithmeticException e) {
            assertTrue(true);
        }

        assertEquals(new Clock(0, 1), new Clock(0.00));
        assertEquals(new Clock(7, 4), new Clock(1.75));
        assertEquals(new Clock(2, 3), new Clock(2. / 3.));

        try {
            new Clock(1459. / 1143.);
            assertTrue(false);
        } catch (ArithmeticException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testNegative() {
        assertEquals(-1.1, new Clock(-1.1).getAsDouble(), EPSILON);
    }

    @Test
    public void testAsDouble() {
        assertEquals(2., new Clock(4, 2).getAsDouble(), EPSILON);
        assertEquals(10. / 3., new Clock(10, 3).getAsDouble(), EPSILON);
    }

    @Test
    public void testAdd() {
        assertEquals(new Clock(5.15), new Clock(3).add(new Clock(2.15)));
        assertEquals(new Clock(5.08), new Clock(6.18).add(new Clock(-1.10)));
    }

    @Test
    public void testSubtract() {
        assertEquals(new Clock(0.85), new Clock(3).subtract(new Clock(2.15)));
        assertEquals(new Clock(7.28), new Clock(6.18).subtract(new Clock(-1.10)));
    }

    @Test
    public void testMin() {
        assertEquals(new Clock(5.34), new Clock(6.71).min(new Clock(5.34)));
    }

    @Test
    public void testMod() {
        assertEquals(new Clock(1.34), new Clock(5.34).mod(4));
    }

    @Test
    public void testComparable() {
        assertTrue(new Clock(4).compareTo(new Clock(9, 2)) < 0);
        assertTrue(new Clock(4).compareTo(new Clock(8, 2)) == 0);
        assertTrue(new Clock(9, 2).compareTo(new Clock(4)) > 0);
    }

    @Test
    public void testEqualsAndHash() {
        Random rand = new Random();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                int divider = 1 + rand.nextInt(20);
                Clock c1 = new Clock(i, divider);
                Clock c2 = new Clock(j, divider);

                assertEquals(i == j, c1.equals(c2));
                assertTrue(i != j || (c1.hashCode() == c2.hashCode()));
            }
        }
    }

    @Test
    public void testToString() {
        String s = new Clock(7, 3).toString();
        assertTrue(s != null);
        assertTrue(s.length() > 0);
    }

    @Test
    public void testSimplify() {
        assertTrue(new Clock(8, 3).equals(new Clock(16, 6)));
    }

    @Test
    public void testHighdesCommonDivider() {
        assertEquals(1, Clock.getHighestCommonDivider(-11, 10));
        assertEquals(4, Clock.getHighestCommonDivider(4, 16));
        assertEquals(5, Clock.getHighestCommonDivider(10, 15));
        assertEquals(1, Clock.getHighestCommonDivider(7, 12));
        assertEquals(12, Clock.getHighestCommonDivider(0, 12));
    }

    @Test
    public void testIsBar() {
        assertEquals(true, new Clock(0, 1).isBar());
        assertEquals(true, new Clock(0, 4).isBar());
        assertEquals(true, new Clock(4, 1).isBar());
        assertEquals(true, new Clock(16, 4).isBar());
        assertEquals(false, new Clock(1, 4).isBar());
        assertEquals(false, new Clock(15, 4).isBar());
        assertEquals(false, new Clock(17, 4).isBar());
    }

    @Test
    public void testIsBeat() {
        assertEquals(true, new Clock(0, 1).isBeat());
        assertEquals(true, new Clock(1, 1).isBeat());
        assertEquals(true, new Clock(2, 1).isBeat());
        assertEquals(true, new Clock(3, 1).isBeat());
        assertEquals(true, new Clock(4, 1).isBeat());
        assertEquals(false, new Clock(1, 4).isBeat());
        assertEquals(false, new Clock(15, 4).isBeat());
    }
}
