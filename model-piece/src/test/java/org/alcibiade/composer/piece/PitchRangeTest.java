package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.*;

public class PitchRangeTest {

    @Test
    public void testPitchRange() {
        new PitchRange(Pitch.C4, Pitch.C5);
        new PitchRange(Pitch.C5, Pitch.C4);
    }

    @Test
    public void testGetLow() {
        PitchRange range = new PitchRange(Pitch.C4, Pitch.C5);
        assertEquals(Pitch.C4, range.getLow());
    }

    @Test
    public void testGetHigh() {
        PitchRange range = new PitchRange(Pitch.C4, Pitch.C5);
        assertEquals(Pitch.C5, range.getHigh());
    }

    @Test
    public void testContains() {
        PitchRange range = new PitchRange(Pitch.C4, Pitch.C5);
        assertTrue(!range.contains(Pitch.E3));
        assertTrue(range.contains(Pitch.C4));
        assertTrue(range.contains(Pitch.A4));
        assertTrue(range.contains(Pitch.C5));
        assertTrue(!range.contains(Pitch.E5));
        assertTrue(!range.contains(Pitch.C6));
    }

    @Test
    public void testTranspose() {
        PitchRange range = new PitchRange(Pitch.C4, Pitch.C5);
        assertEquals(Pitch.A4, range.transpose(Pitch.A2));
        assertEquals(Pitch.A4, range.transpose(Pitch.A3));
        assertEquals(Pitch.A4, range.transpose(Pitch.A4));
        assertEquals(Pitch.A4, range.transpose(Pitch.A5));
    }

    @Test
    public void testToString() {
        PitchRange range = new PitchRange(Pitch.C4, Pitch.C5);
        assertNotNull(range.toString());
    }
}
