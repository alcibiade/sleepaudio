package org.alcibiade.composer.piece;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScaleTest {

    private static int sumIntervals(Scale scale) {
        int result = 0;

        for (Interval interval : scale.getIntervals()) {
            result += interval.getHalfTones();
        }

        return result;
    }

    /**
     * Test constant scales
     */
    @Test
    public void testScales() {
        assertEquals(12, sumIntervals(Scale.MAJOR));
        assertEquals(12, sumIntervals(Scale.MINOR));
    }

    /*
     * Test method for 'org.alcibiade.composer.Scale.Scale(Interval[])'
     */
    @Test
    public void testScale() {
        Interval[] intervals = {Interval.FIFTH, Interval.FOURTH};
        new Scale("Test scale", intervals);
    }

    /*
     * Test method for 'org.alcibiade.composer.Scale.getIntervals()'
     */
    @Test
    public void testGetIntervals() {
        Interval[] intervals = {Interval.FIFTH, Interval.FOURTH};
        Scale sc = new Scale("Test scale", intervals);
        List<Interval> intervalsList = sc.getIntervals();
        assertEquals(2, intervalsList.size());
        assertEquals(Interval.FIFTH, intervalsList.get(0));
        assertEquals(Interval.FOURTH, intervalsList.get(1));
    }

    @Test
    public void testGetDegreeInterval() {
        assertEquals(Interval.FIFTH.getHalfTones(), Scale.MAJOR.getDegreeInterval(4).getHalfTones());
        assertEquals(-Interval.FOURTH.getHalfTones(), Scale.MAJOR.getDegreeInterval(-3).getHalfTones());
    }

    @Test
    public void testGetName() {
        Interval[] intervals = {Interval.FIFTH, Interval.FOURTH};
        Scale sc = new Scale("Test scale", intervals);
        assertEquals("Test scale", sc.getName());
    }

    @Test
    public void testToString() {
        assertNotNull(Scale.MAJOR.toString());
    }
}
