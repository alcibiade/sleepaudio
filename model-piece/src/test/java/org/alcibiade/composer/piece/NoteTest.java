package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NoteTest {

    @Test
    public void testNote() {
        new Note(Pitch.C3, new Clock(2.5), new Velocity(2_000));
        new Note(Pitch.E5, new Clock(1.25), new Velocity(3_000));
    }

    @Test
    public void testGetDuration() {
        Note note = new Note(Pitch.C3, new Clock(2.5), new Velocity(2_000));
        assertEquals(new Clock(2.5), note.getDuration());
    }

    @Test
    public void testGetPitch() {
        Note note = new Note(Pitch.C3, new Clock(2.5), new Velocity(2_000));
        assertEquals(Pitch.C3, note.getPitch());
    }

    @Test
    public void testGetVelocity() {
        Note note = new Note(Pitch.C3, new Clock(2.5), new Velocity(2_000));
        assertEquals(new Velocity(2_000), note.getVelocity());
    }
}
