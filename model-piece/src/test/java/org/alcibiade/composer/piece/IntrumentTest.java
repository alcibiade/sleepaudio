package org.alcibiade.composer.piece;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class IntrumentTest {

    @Test
    public void testInstrument() {
        new Instrument("Dummy instrument", 1, 1, PitchRange.ALTO);
    }

    @Test
    public void testGetOrchestraId() {
        Instrument i = new Instrument("Dummy instrument", 39, 1, PitchRange.ALTO);
        assertEquals(39, i.getOrchestraId());
    }

    @Test
    public void testGetRange() {
        Instrument i = new Instrument("Dummy instrument", 39, 1, PitchRange.ALTO);
        assertEquals(PitchRange.ALTO, i.getRange());
    }

    @Test
    public void testGetName() {
        Instrument i = new Instrument("Dummy instrument", 39, 1, PitchRange.ALTO);
        assertEquals("Dummy instrument", i.getName());
    }

    @Test
    public void testGetGM() {
        Instrument i = new Instrument("Dummy instrument", 39, 48, PitchRange.ALTO);
        assertEquals(48, i.getGMCode());
    }

    @Test
    public void testToString() {
        Instrument i = new Instrument("Dummy instrument", 39, 1, PitchRange.ALTO);
        assertNotNull(i.toString());
    }
}
