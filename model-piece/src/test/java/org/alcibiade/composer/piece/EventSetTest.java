package org.alcibiade.composer.piece;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class EventSetTest {

    @Test
    public void testPitches() {
        EventSet<Event<Note>> set = new EventSet<>();
        set.add(new Event<>(new Clock(0.), new Note(Pitch.A4, new Clock(1),
                Velocity.FF)));
        set.add(new Event<>(new Clock(1.), new Note(Pitch.C3, new Clock(1),
                Velocity.FF)));

        Set<Pitch> pitches = set.getPitches();
        assertEquals(2, pitches.size());
        assertTrue(pitches.contains(Pitch.A4));
        assertTrue(pitches.contains(Pitch.C3));
        assertFalse(pitches.contains(Pitch.A3));
    }

}
