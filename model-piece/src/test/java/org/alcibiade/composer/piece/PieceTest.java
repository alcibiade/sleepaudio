package org.alcibiade.composer.piece;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PieceTest {

    private static final Note n1 = new Note(Pitch.C3, new Clock(1.),
            Velocity.FF);
    private static final Note n2 = new Note(Pitch.E3, new Clock(1.),
            Velocity.FF);
    private static final Note n3 = new Note(Pitch.G3, new Clock(1.),
            Velocity.FF);
    private Piece samplePiece;
    private Track<Event<Note>> track1;
    private Track<Event<Note>> track2;

    @Before
    public void setUp() {

        samplePiece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));

        track1 = new Track<>("Track1", Instrument.GLOCKENSPIEL);
        track1.addEvent(new Event<>(new Clock(1.), n1));
        track1.addEvent(new Event<>(new Clock(2.), n2));
        track1.addEvent(new Event<>(new Clock(3.), n3));
        samplePiece.addTrack(track1);

        track2 = new Track<>("Track2", Instrument.PAD);
        track2.addEvent(new Event<>(new Clock(1.), n3));
        track2.addEvent(new Event<>(new Clock(2.), n2));
        track2.addEvent(new Event<>(new Clock(3.), n1));
        samplePiece.addTrack(track2);
    }

    @Test
    public void testPiece() {
        new Piece("Sample piece", new Key(Pitch.MID_A, Scale.MINOR), 90,
                new Clock(120));
    }

    @Test
    public void testGetKey() {
        Key key = new Key(Pitch.MID_A, Scale.MINOR);
        Piece piece = new Piece("Sample piece", key, 90, new Clock(120));
        assertEquals(key, piece.getKey());
    }

    @Test
    public void testGetDuration() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));
        assertEquals(new Clock(120.), piece.getDuration());
    }

    @Test
    public void testGetTempo() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));
        assertEquals(90., piece.getTempo(), 0.000001);
    }

    @Test
    public void testGetTitle() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));
        assertEquals("Sample piece", piece.getTitle());
    }

    @Test
    public void testGetTracks() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));

        assertEquals(0, piece.getNoteTracks().size());
        piece.addTrack(track1);
        assertEquals(1, piece.getNoteTracks().size());
        assertEquals(track1, piece.getNoteTracks().iterator().next());
    }

    @Test
    public void testAddTrack() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));

        assertEquals(0, piece.getNoteTracks().size());
        piece.addTrack(track1);
        assertEquals(1, piece.getNoteTracks().size());
        assertEquals(track1, piece.getNoteTracks().iterator().next());
    }

    @Test
    public void testRemoveTrack() {
        Piece piece = new Piece("Sample piece", new Key(Pitch.MID_A,
                Scale.MINOR), 90, new Clock(120));

        piece.addTrack(track1);
        piece.addTrack(track2);
        assertEquals(2, piece.getNoteTracks().size());
        piece.removeTrack(track1);
        assertEquals(1, piece.getNoteTracks().size());
        assertEquals(track2, piece.getNoteTracks().iterator().next());
    }

    @Test
    public void testGetPitchesDouble() {
        assertEquals(0, samplePiece.getEvents(new Clock(0.0)).getPitches().size());
        assertEquals(0, samplePiece.getEvents(new Clock(0.5)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(1.0)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(1.5)).getPitches().size());
        assertEquals(1, samplePiece.getEvents(new Clock(2.0)).getPitches().size());
        assertEquals(1, samplePiece.getEvents(new Clock(2.5)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(3.0)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(3.5)).getPitches().size());
        assertEquals(0, samplePiece.getEvents(new Clock(4.0)).getPitches().size());
    }

    @Test
    public void testGetPitchesDoubleDouble() {
        assertEquals(0, samplePiece.getEvents(new Clock(0.0), new Clock(0.5)).getPitches().size());
        assertEquals(0, samplePiece.getEvents(new Clock(0.5), new Clock(1.0)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(1.0), new Clock(1.5)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(1.5), new Clock(2.0)).getPitches().size());
        assertEquals(1, samplePiece.getEvents(new Clock(2.0), new Clock(2.5)).getPitches().size());
        assertEquals(1, samplePiece.getEvents(new Clock(2.5), new Clock(3.0)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(3.0), new Clock(3.5)).getPitches().size());
        assertEquals(2, samplePiece.getEvents(new Clock(3.5), new Clock(4.0)).getPitches().size());
        assertEquals(0, samplePiece.getEvents(new Clock(4.0), new Clock(4.5)).getPitches().size());

        assertEquals(3, samplePiece.getEvents(new Clock(1.5), new Clock(2.5)).getPitches().size());
        assertEquals(3, samplePiece.getEvents(new Clock(2.5), new Clock(3.5)).getPitches().size());
    }

    @Test
    public void testGetPitchesDoubleDoubleInstrument() {
        assertEquals(0, samplePiece.getEvents(new Clock(0.0), new Clock(3.5),
                Instrument.VOICEPAD, true).getPitches().size());
        assertEquals(3, samplePiece.getEvents(new Clock(0.0), new Clock(3.5),
                Instrument.VOICEPAD, false).getPitches().size());

        assertEquals(3, samplePiece.getEvents(new Clock(0.0), new Clock(3.5),
                Instrument.GLOCKENSPIEL, true).getPitches().size());
        assertEquals(3, samplePiece.getEvents(new Clock(0.0), new Clock(3.5),
                Instrument.GLOCKENSPIEL, false).getPitches().size());

    }
}
