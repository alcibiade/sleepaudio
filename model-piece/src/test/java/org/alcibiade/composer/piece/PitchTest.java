package org.alcibiade.composer.piece;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.*;

public class PitchTest {

    /*
     * Test method for 'org.alcibiade.composer.Pitch.Pitch(double)'
     */
    @Test
    public void testPitch() {
        Pitch p = new Pitch(220.);
        assertEquals(220., p.getFrequency(), 0.01);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.getFrequency()'
     */
    @Test
    public void testGetFrequency() {
        Pitch p = new Pitch(220.);
        assertEquals(220., p.getFrequency(), 0.01);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.add(Interval)'
     */
    @Test
    public void testAdd() {
        Pitch p = new Pitch(220);
        p = p.add(new Interval(12));
        assertEquals(440., p.getFrequency(), 0.01);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.subtract(Interval)'
     */
    @Test
    public void testSubtract() {
        Pitch p = new Pitch(440);
        p = p.subtract(new Interval(12));
        assertEquals(220., p.getFrequency(), 0.01);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.toString()'
     */
    @Test
    public void testToString() {
        assertNotNull(Pitch.MID_A.toString());
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.transpose(Pitch)'
     */
    @Test
    public void testTranspose() {
        assertEquals(Pitch.G2, Pitch.G2.transpose(Pitch.G2));
        assertEquals(Pitch.E3, Pitch.E2.transpose(Pitch.G2));
        assertEquals(Pitch.E3, Pitch.E5.transpose(Pitch.G2));
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.compareTo(Pitch)'
     */
    @Test
    public void testCompareTo() {

        // --- Basic notes tests

        assertTrue(Pitch.C3.compareTo(Pitch.C3) == 0);
        assertTrue(Pitch.C3.compareTo(Pitch.E3) < 0);
        assertTrue(Pitch.C3.compareTo(Pitch.E2) > 0);

        // --- Test that rounding does not alter consistency

        Pitch g3 = Pitch.C3.add(Interval.FIFTH);
        Pitch c3 = Pitch.C3.add(Interval.FIFTH).add(
                new Interval(-Interval.FIFTH.getHalfTones()));

        assertTrue(c3.compareTo(Pitch.C3) == 0);
        assertTrue(c3.compareTo(g3) < 0);

        // --- Test rounding

        Pitch e3bu = new Pitch(Pitch.E3.getFrequency() + Pitch.EPSILON * 0.4);
        Pitch e3bd = new Pitch(Pitch.E3.getFrequency() - Pitch.EPSILON * 0.4);
        assertTrue(Pitch.E3.compareTo(e3bu) == 0);
        assertTrue(Pitch.E3.compareTo(e3bd) == 0);
        assertTrue(e3bd.compareTo(e3bu) == 0);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.equals(Object)'
     */
    @Test
    public void testEquals() {

        // --- Basic notes tests

        Assertions.assertThat(Pitch.C3)
                .isEqualTo(Pitch.C3)
                .isNotEqualTo(Pitch.E3)
                .isNotEqualTo(Pitch.E2)
                .isNotEqualTo(Pitch.C2);

        // --- Test that rounding does not alter consistency

        Pitch g3 = Pitch.C3.add(Interval.FIFTH);
        Pitch c3 = Pitch.C3.add(Interval.FIFTH).add(
                new Interval(-Interval.FIFTH.getHalfTones()));

        Assertions.assertThat(c3).isEqualTo(Pitch.C3).isNotEqualTo(g3);

        // --- Test rounding

        Pitch e3bu = new Pitch(Pitch.E3.getFrequency() + Pitch.EPSILON * 0.4);
        Pitch e3bd = new Pitch(Pitch.E3.getFrequency() - Pitch.EPSILON * 0.4);
        Assertions.assertThat(Pitch.E3)
                .isEqualTo(e3bu)
                .isEqualTo(e3bd);

        Assertions.assertThat(e3bu).isEqualTo(e3bd);
    }

    /*
     * Test method for 'org.alcibiade.composer.Pitch.hashCode()'
     */
    @Test
    public void testHashCode() {

        // --- Basic functionnal tests

        assertTrue(Pitch.C3.hashCode() != Pitch.E3.hashCode());
        assertTrue(Pitch.C3.hashCode() < Pitch.C4.hashCode());
        assertTrue(Pitch.C2.hashCode() < Pitch.E3.hashCode());

        // --- Test consistency between successive calls

        assertEquals(Pitch.C3.hashCode(), Pitch.C3.hashCode());

        // --- Test consistency between instances

        Pitch c3 = new Pitch(Pitch.C3.getFrequency());
        assertEquals(Pitch.C3.hashCode(), c3.hashCode());

        // --- Test rounding

        Pitch e3bu = new Pitch(Pitch.E3.getFrequency() + Pitch.EPSILON * 0.6);
        Pitch e3bd = new Pitch(Pitch.E3.getFrequency() - Pitch.EPSILON * 0.6);
        assertEquals(e3bd.hashCode() + 1, e3bu.hashCode());
    }

    @Test
    public void testIsChromatic() {
        assertTrue(Pitch.A4.isChromatic());
        Pitch p1 = new Pitch(Pitch.A4.getFrequency() + 1.0);
        assertFalse(p1.isChromatic());
    }
}
