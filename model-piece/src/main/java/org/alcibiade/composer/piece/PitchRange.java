package org.alcibiade.composer.piece;

public class PitchRange {

    public static final PitchRange SOPRANO = new PitchRange(Pitch.MID_C,
            Pitch.SOPRANO_C);
    public static final PitchRange ALTO = new PitchRange(Pitch.G3, Pitch.E5);
    public static final PitchRange TENOR = new PitchRange(Pitch.C3, Pitch.C5);
    public static final PitchRange BARITONE = new PitchRange(Pitch.G2, Pitch.G4);
    public static final PitchRange BASS = new PitchRange(Pitch.E2, Pitch.E4);
    public static final PitchRange FULL = new PitchRange(Pitch.C2, Pitch.C6);
    private Pitch low;
    private Pitch high;

    public PitchRange(Pitch low, Pitch high) {
        this.low = low;
        this.high = high;
    }

    public Pitch getLow() {
        return low;
    }

    public Pitch getHigh() {
        return high;
    }

    public boolean contains(Pitch pitch) {
        return pitch.getFrequency() >= low.getFrequency()
                && pitch.getFrequency() <= high.getFrequency();
    }

    public Pitch transpose(Pitch pitch) {
        while (pitch.compareTo(low) < 0) {
            pitch = pitch.add(Interval.OCTAVE);
        }

        while (pitch.compareTo(high) > 0) {
            pitch = pitch.subtract(Interval.OCTAVE);
        }

        return pitch;
    }

    @Override
    public String toString() {
        return "Pitch[" + low + "-" + high + "]";
    }
}
