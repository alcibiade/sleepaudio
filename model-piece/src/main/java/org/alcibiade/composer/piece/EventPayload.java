package org.alcibiade.composer.piece;

/**
 * Behevior of event-attached items.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public interface EventPayload {

    /**
     * Get the duration of the item.
     *
     * @return the duration expressed as a clock instance
     */
    Clock getDuration();
}
