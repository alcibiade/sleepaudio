package org.alcibiade.composer.piece;

public class ChromaticPitch {

    private char noteLetter;
    private boolean sharp;
    private int octave;

    public ChromaticPitch(char noteLetter, boolean sharp, int octave) {
        super();
        this.noteLetter = noteLetter;
        this.sharp = sharp;
        this.octave = octave;
    }

    public char getNoteLetter() {
        return noteLetter;
    }

    public boolean isSharp() {
        return sharp;
    }

    public int getOctave() {
        return octave;
    }

    @Override
    public String toString() {
        return String.format("%s%s%d", noteLetter, sharp ? "#" : "", octave);
    }
}
