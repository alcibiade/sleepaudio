package org.alcibiade.composer.piece;

import java.util.LinkedHashSet;
import java.util.Set;

public class Piece {

    private String title;
    private double tempo;
    private Clock duration;
    private Key key;
    private Track<Event<ConductorInstructions>> conductorTrack = new Track<>("Conductor");
    private Set<Track<Event<Note>>> noteTracks = new LinkedHashSet<>();

    public Piece(String title, Key key, double tempo, Clock duration) {
        this.title = title;
        this.tempo = tempo;
        this.key = key;
        this.duration = duration;
    }

    public Key getKey() {
        return key;
    }

    public Clock getDuration() {
        return duration;
    }

    public double getTempo() {
        return tempo;
    }

    public String getTitle() {
        return title;
    }

    public Track<Event<ConductorInstructions>> getConductorTrack() {
        return conductorTrack;
    }

    public Set<Track<Event<Note>>> getNoteTracks() {
        return noteTracks;
    }

    public void addTrack(Track<Event<Note>> track) {
        noteTracks.add(track);
    }

    public boolean removeTrack(Track<Event<Note>> track) {
        return noteTracks.remove(track);
    }

    public EventSet<Event<Note>> getEvents(Clock position) {
        EventSet<Event<Note>> eventset = new EventSet<>();

        noteTracks.stream().forEach((track) -> {
            eventset.addAll(track.getEvents(position));
        });

        return eventset;
    }

    public EventSet<Event<Note>> getEvents(Clock posStart, Clock posEnd) {
        EventSet<Event<Note>> eventset = new EventSet<>();

        noteTracks.stream().forEach((track) -> {
            eventset.addAll(track.getEvents(posStart, posEnd));
        });

        return eventset;
    }

    public EventSet<Event<Note>> getEvents(Clock posStart, Clock posEnd,
            Instrument instrument, boolean include) {
        EventSet<Event<Note>> eventset = new EventSet<>();

        noteTracks.stream()
                .filter((track) -> (include == (track.getInstrument().equals(instrument))))
                .forEach((track) -> {
                    eventset.addAll(track.getEvents(posStart, posEnd));
                });

        return eventset;
    }
}
