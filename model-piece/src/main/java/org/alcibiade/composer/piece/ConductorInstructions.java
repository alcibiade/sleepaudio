package org.alcibiade.composer.piece;

/**
 * Conductor events provide silent structural data about the piece used by the composers.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class ConductorInstructions implements EventPayload {

    private Clock duration;

    public ConductorInstructions(Clock duration) {
        this.duration = duration;
    }

    @Override
    public Clock getDuration() {
        return duration;
    }
}
