package org.alcibiade.composer.piece;

public class Pitch implements Comparable<Pitch> {

    private static final char[] NOTE_LETTERS = {'C', 'C', 'D', 'D', 'E', 'F',
        'F', 'G', 'G', 'A', 'A', 'B'};
    private static final boolean[] NOTE_SHARPS = {false, true, false, true,
        false, false, true, false, true, false, true, false};
    /**
     * This is the frequency threshold to decide whether pitches are the equal
     * or not.
     */
    public static final double EPSILON = 0.01;
    public static final Pitch A4 = new Pitch(440.00);
    public static final Pitch C4 = A4.add(new Interval(-9));
    public static final Pitch C2 = C4.add(new Interval(-24));
    public static final Pitch E2 = C2.add(new Interval(4));
    public static final Pitch G2 = C2.add(new Interval(7));
    public static final Pitch A2 = C2.add(new Interval(9));
    public static final Pitch C3 = C4.add(new Interval(-12));
    public static final Pitch E3 = C3.add(new Interval(4));
    public static final Pitch G3 = C3.add(new Interval(7));
    public static final Pitch A3 = C3.add(new Interval(9));
    public static final Pitch E4 = C4.add(new Interval(4));
    public static final Pitch G4 = C4.add(new Interval(7));
    public static final Pitch C5 = C4.add(new Interval(12));
    public static final Pitch E5 = C5.add(new Interval(4));
    public static final Pitch A5 = C5.add(new Interval(9));
    public static final Pitch C6 = C4.add(new Interval(24));
    public static final Pitch MID_A = A4;
    public static final Pitch MID_C = C4;
    public static final Pitch SOPRANO_C = C6;
    private double frequency;

    public Pitch(double frequency) {
        this.frequency = frequency;
    }

    public double getFrequency() {
        return frequency;
    }

    public Pitch add(Interval interval) {
        Pitch result = new Pitch(frequency
                * Math.pow(2., interval.getHalfTones() / 12.));
        return result;
    }

    public Pitch subtract(Interval interval) {
        Pitch result = new Pitch(frequency
                * Math.pow(2., -interval.getHalfTones() / 12.));
        return result;
    }

    public Pitch transpose(Pitch base) {
        double transFreq = frequency;

        while (transFreq < base.frequency) {
            transFreq *= 2;
        }

        while (transFreq > base.frequency * 2) {
            transFreq /= 2;
        }

        return new Pitch(transFreq);
    }

    public double getPitchIndex() {
        double factor = 12D / Math.log(2D);
        double pitchIndex = 57D + factor * Math.log(this.frequency / 440);
        return pitchIndex;
    }

    public boolean isChromatic() {
        double pitchIndex = getPitchIndex();
        return Math.abs(pitchIndex - Math.floor(pitchIndex)) < EPSILON;
    }

    public ChromaticPitch getChromaticInformations() {
        int i = (int) (getPitchIndex() + 120.5D);
        int octaveIndex = i % 12;
        int octave = i / 12 - 10;
        return new ChromaticPitch(NOTE_LETTERS[octaveIndex],
                NOTE_SHARPS[octaveIndex], octave);
    }

    @Override
    public String toString() {
        String[] notenames = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#",
            "A", "A#", "B"};

        StringBuilder octava = new StringBuilder();
        int note = 0;

        double freq = frequency;
        double midc = 440. * Math.pow(2., -9. / 12.);

        while (freq < midc) {
            freq *= 2.;
            octava.append(",");
        }

        while (freq >= midc * 2) {
            freq /= 2.;
            octava.append("'");
        }

        freq -= EPSILON / 2;

        while (freq > midc) {
            note += 1;
            freq /= Math.pow(2, 0.5 / 6.0);
        }

        return String.format("%8.3fHz/%s", frequency, notenames[note] + octava);
    }

    @Override
    public int compareTo(Pitch otherPitch) {
        int result = 0;

        if (Math.abs(frequency - otherPitch.frequency) > EPSILON) {
            Double myfreq = this.frequency;
            Double opfreq = otherPitch.frequency;
            result = myfreq.compareTo(opfreq);
        }

        return result;
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;

        if (o instanceof Pitch) {
            Pitch op = (Pitch) o;
            result = compareTo(op) == 0;
        }

        return result;
    }

    @Override
    public int hashCode() {
        return (int) (frequency / EPSILON);
    }
}
