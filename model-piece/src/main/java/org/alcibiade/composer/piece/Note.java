package org.alcibiade.composer.piece;

public class Note implements EventPayload {

    private Pitch pitch;
    private Velocity velocity;
    private Clock duration;

    public Note(Pitch pitch, Clock duration, Velocity velocity) {
        this.pitch = pitch;
        this.duration = duration;
        this.velocity = velocity;
    }

    @Override
    public Clock getDuration() {
        return duration;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public Velocity getVelocity() {
        return velocity;
    }

    @Override
    public String toString() {
        return "Note{" +
                "pitch=" + pitch +
                ", velocity=" + velocity +
                ", duration=" + duration +
                '}';
    }
}
