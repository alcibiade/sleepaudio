package org.alcibiade.composer.piece;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class EventSet<T extends Event> extends LinkedHashSet<T> {

    private static final long serialVersionUID = 1L;

    public Set<Pitch> getPitches() {
        Set<Pitch> pitches = new HashSet<>();

        this.stream()
                .map(Event::getItem)
                .filter((item) -> (item instanceof Note))
                .forEach((item) -> pitches.add(((Note) item).getPitch()));

        return pitches;
    }
}
