package org.alcibiade.composer.piece;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@ToString
@EqualsAndHashCode
public class Clock implements Comparable<Clock> {

    public static final double EPSILON = 0.00001;
    public static final Clock ZERO = new Clock(0);
    private static final int MAX_INITIAL_DIVIDER = 256;
    private int value;
    private int divider;

    public Clock() {
        this(0);
    }

    public Clock(int value) {
        this(value, 1);
    }

    public Clock(double value) {
        int d;

        for (d = 1; d < MAX_INITIAL_DIVIDER; d++) {
            if ((Math.abs(value) * d) - Math.floor(Math.abs(value) * d) < EPSILON) {
                break;
            }
        }

        if (d == MAX_INITIAL_DIVIDER) {
            throw new ArithmeticException("Can't represent double value "
                + value);
        }

        this.value = (int) (value * d);
        this.divider = d;

        simplify();
    }

    public Clock(int value, int divider) {
        if (divider == 0) {
            throw new ArithmeticException("Clock divider can't be zero");
        }

        this.value = value;
        this.divider = divider;

        simplify();
    }

    public boolean isBeat() {
        return divider == 1;
    }

    public boolean isBar() {
        return divider == 1 && value % 4 == 0;
    }

    public double getAsDouble() {
        return (double) value / (double) divider;
    }

    public Clock add(Clock duration) {
        return new Clock(value * duration.divider + duration.value * divider,
            divider * duration.divider);
    }

    public Clock subtract(Clock duration) {
        return new Clock(value * duration.divider - duration.value * divider,
            divider * duration.divider);
    }

    public Clock min(Clock clock) {
        Clock result = null;

        if (this.compareTo(clock) < 0) {
            result = this;
        } else {
            result = clock;
        }

        return result;
    }

    public Clock mod(int i) {
        return new Clock(value % (i * divider), divider);
    }

    @Override
    public int compareTo(Clock clock) {
        int result = 0;

        if (!this.equals(clock)) {
            double diff = getAsDouble() - clock.getAsDouble();
            if (diff < 0) {
                result = -1;
            } else {
                result = 1;
            }
        }

        return result;
    }

    private void simplify() {
        int commonDivider = getHighestCommonDivider(value, divider);
        value /= commonDivider;
        divider /= commonDivider;
    }

    protected static int getHighestCommonDivider(int a, int b) {
        int highestDivider = 0;

        if (a == 0) {
            highestDivider = b;
        } else {
            Set<Integer> dividersA = getDividers(a);
            Set<Integer> dividersB = getDividers(b);

            for (Integer divider : dividersA) {
                if (dividersB.contains(divider) && divider > highestDivider) {
                    highestDivider = divider;
                }
            }
        }

        assert highestDivider > 0;

        return highestDivider;
    }

    private static Set<Integer> getDividers(int i) {
        Set<Integer> dividers = new HashSet<>();
        dividers.add(1);

        for (int d = Math.abs(i); d > 1; d--) {
            double q = (double) Math.abs(i) / d;
            if (q - (int) q < EPSILON) {
                dividers.add(d);
            }
        }

        return dividers;
    }
}
