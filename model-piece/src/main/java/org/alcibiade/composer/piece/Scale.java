package org.alcibiade.composer.piece;

import java.util.Arrays;
import java.util.List;

public class Scale {

    public static final Scale MINOR = new Scale("Minor", Interval.TONE, Interval.HALF_TONE,
            Interval.TONE, Interval.TONE, Interval.HALF_TONE,
            Interval.TONE, Interval.TONE);
    public static final Scale MAJOR = new Scale("Major", Interval.TONE, Interval.TONE,
            Interval.HALF_TONE, Interval.TONE, Interval.TONE,
            Interval.TONE, Interval.HALF_TONE);
    private List<Interval> intervals;
    private String name;

    public Scale(String name, Interval... intervals) {
        this.name = name;
        this.intervals = Arrays.asList(intervals);
    }

    public String getName() {
        return name;
    }

    public List<Interval> getIntervals() {
        return intervals;
    }

    public Interval getDegreeInterval(int degree) {
        int degreesLeft = degree;
        Interval interval = new Interval(0);

        while (degreesLeft < 0) {
            degreesLeft += intervals.size();
            interval = interval.subtract(Interval.OCTAVE);
        }

        for (int i = 0; i < degreesLeft; i++) {
            interval = interval.add(intervals.get(i % intervals.size()));
        }

        return interval;
    }

    @Override
    public String toString() {
        return name;
    }
}
