package org.alcibiade.composer.piece;

import java.util.Iterator;

public class Key {

    private Pitch tonic;
    private Scale scale;

    public Key(Pitch tonic, Scale scale) {
        this.tonic = tonic;
        this.scale = scale;
    }

    public Pitch getTonic() {
        return tonic;
    }

    public Scale getScale() {
        return scale;
    }

    public boolean contains(Pitch pitch) {
        boolean result = false;
        Pitch transposed = pitch.transpose(tonic);

        Pitch step = tonic;
        Iterator<Interval> intervals = scale.getIntervals().iterator();
        while (!result && intervals.hasNext()) {
            Interval interval = intervals.next();
            result = transposed.equals(step);
            step = step.add(interval);
        }

        return result;
    }

    @Override
    public String toString() {
        return tonic + " " + scale;
    }
}
