package org.alcibiade.composer.piece;

public class Instrument {

    public static final Instrument SINE = new Instrument("Sine Wave", 1, 80, PitchRange.FULL);
    public static final Instrument PAD = new Instrument("Pad", 10, 89, PitchRange.BARITONE);
    public static final Instrument VOICEPAD = new Instrument("Voice Pad", 11, 91, PitchRange.FULL);
    public static final Instrument GLOCKENSPIEL = new Instrument("Glockenspiel", 40, 9, PitchRange.FULL);
    public static final Instrument SYNTH_LEAD = new Instrument("Synth Lead", 41, 85, PitchRange.FULL);
    public static final Instrument PIANO = new Instrument("Piano", 41, 0, PitchRange.FULL);
    private String name;
    private int orchestra;
    private int midi;
    private PitchRange range;

    public Instrument(String name, int orchestra, int midi, PitchRange range) {
        this.name = name;
        this.orchestra = orchestra;
        this.midi = midi;
        this.range = range;
    }

    public int getOrchestraId() {
        return orchestra;
    }

    public int getGMCode() {
        return midi;
    }

    public PitchRange getRange() {
        return range;
    }

    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        boolean result = false;

        if (o instanceof Instrument) {
            Instrument instrument = (Instrument) o;
            result = name.equals(instrument.name);
        }

        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return name;
    }
}
