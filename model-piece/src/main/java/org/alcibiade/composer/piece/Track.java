package org.alcibiade.composer.piece;

import java.util.LinkedHashSet;
import java.util.Set;

public class Track<T extends Event> {

    private String name = null;
    private Instrument instrument = null;
    private Set<T> events = new LinkedHashSet<>();

    public Track(String name) {
        this(name, null);
    }

    public Track(String name, Instrument instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public EventSet<T> getEvents() {
        EventSet<T> eventSet = new EventSet<>();
        eventSet.addAll(events);
        return eventSet;
    }

    public EventSet<T> getEvents(Clock position) {
        EventSet<T> eventSet = new EventSet<>();

        for (T event : this.events) {
            if (event.getTime().compareTo(position) <= 0
                    && event.getTime().add(event.getDuration()).compareTo(
                            position) > 0) {
                eventSet.add(event);
            }
        }

        return eventSet;
    }

    public EventSet<T> getEvents(Clock posStart, Clock posEnd) {
        EventSet<T> eventSet = new EventSet<>();

        for (T event : this.events) {
            if (event.getTime().compareTo(posEnd) < 0
                    && event.getTime().add(event.getDuration()).compareTo(
                            posStart) > 0) {
                eventSet.add(event);
            }
        }

        return eventSet;
    }

    public void addEvent(T event) {
        events.add(event);
    }

    public boolean removeEvent(T event) {
        return events.remove(event);
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("Track{%s}", name);
    }

}
