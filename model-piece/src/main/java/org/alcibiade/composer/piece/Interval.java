package org.alcibiade.composer.piece;

import lombok.*;

@Builder
@RequiredArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@EqualsAndHashCode
@ToString
public class Interval {

    public static final Interval HALF_TONE = new Interval(1);
    public static final Interval TONE = new Interval(2);
    public static final Interval FOURTH = new Interval(5);
    public static final Interval FIFTH = new Interval(7);
    public static final Interval OCTAVE = new Interval(12);
    private final int halfTones;

    public Interval addHalfTones(int halfTones) {
        return new Interval(this.halfTones + halfTones);
    }

    public Interval subtractHalfTones(int halfTones) {
        return new Interval(this.halfTones - halfTones);
    }

    public Interval add(Interval interval) {
        return new Interval(this.halfTones + interval.halfTones);
    }

    public Interval subtract(Interval interval) {
        return new Interval(this.halfTones - interval.halfTones);
    }
}
