package org.alcibiade.composer.piece;

import org.alcibiade.asciiart.raster.CharacterRaster;
import org.alcibiade.asciiart.raster.ExtensibleCharacterRaster;
import org.alcibiade.asciiart.raster.RasterContext;
import org.alcibiade.asciiart.widget.TextWidget;

public class Event<T extends EventPayload> {

    private Clock time;
    private String comment = null;
    private T item;

    public Event(Clock time, T item) {
        this.time = time;
        this.item = item;
    }

    public Clock getTime() {
        return time;
    }

    public T getItem() {
        return item;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(TextWidget widget) {
        CharacterRaster raster = new ExtensibleCharacterRaster(' ');
        widget.render(new RasterContext(raster));
        this.comment = raster.toString();
    }

    public Clock getDuration() {
        return item.getDuration();
    }

    @Override
    public String toString() {
        return "Event{" +
                "time=" + time +
                ", comment='" + comment + '\'' +
                ", item=" + item +
                '}';
    }
}
