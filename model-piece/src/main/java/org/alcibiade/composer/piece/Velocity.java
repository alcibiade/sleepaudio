package org.alcibiade.composer.piece;

public class Velocity implements Comparable<Velocity> {

    private static final double ATTENUATION = 0.80;
    public static final int MAX_VALUE = 30 * 1_000;
    public static final int DYNAMIC_FF = (int) (MAX_VALUE * ATTENUATION);
    public static final Velocity FF = new Velocity(DYNAMIC_FF);
    public static final int DYNAMIC_F = (int) (DYNAMIC_FF * ATTENUATION);
    public static final Velocity F = new Velocity(DYNAMIC_F);
    public static final int DYNAMIC_MF = (int) (DYNAMIC_F * ATTENUATION);
    public static final Velocity MF = new Velocity(DYNAMIC_MF);
    public static final int DYNAMIC_MP = (int) (DYNAMIC_MF * ATTENUATION);
    public static final Velocity MP = new Velocity(DYNAMIC_MP);
    public static final int DYNAMIC_P = (int) (DYNAMIC_MP * ATTENUATION);
    public static final Velocity P = new Velocity(DYNAMIC_P);
    public static final int DYNAMIC_PP = (int) (DYNAMIC_P * ATTENUATION);
    public static final Velocity PP = new Velocity(DYNAMIC_PP);
    private Integer dynamic;
    private Integer balance;

    public Velocity(int dynamic) {
        this(dynamic, MAX_VALUE / 2);
    }

    public Velocity(int dynamic, int balance) {
        this.dynamic = dynamic;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Velocity:%d/%d", dynamic, balance);
    }

    @Override
    public int compareTo(Velocity v) {
        int result = dynamic.compareTo(v.dynamic);

        if (result == 0) {
            result = balance.compareTo(v.balance);
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof Velocity) {
            Velocity v = (Velocity) obj;
            result = this.dynamic.equals(v.dynamic);
            result = result && this.balance.equals(v.balance);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return 117 * balance.hashCode() + dynamic.hashCode();
    }

    public double getDynamic(double maxValue) {
        return dynamic * maxValue / MAX_VALUE;
    }

    public double getBalance(double maxValue) {
        return balance * maxValue / MAX_VALUE;
    }
}
