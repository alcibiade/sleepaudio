package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * Shift a signal in time.
 */
@Builder
@Getter
@Setter
@ToString
public class Delay implements Generator {

    private Generator input;
    private Generator duration;

    @Override
    public double getGeneratedValue(double clock) {
        double v = 0;
        double delayedClock = clock - duration.getGeneratedValue(clock);

        if (delayedClock >= 0) {
            v = input.getGeneratedValue(delayedClock);
        }

        return v;
    }
}
