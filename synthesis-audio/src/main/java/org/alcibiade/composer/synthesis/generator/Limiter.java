package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * Limit output level.
 */
@Builder
@Getter
@Setter
@ToString
public class Limiter implements Generator {
    private Generator input;
    private Generator level;
    private Generator limit;

    @Override
    public double getGeneratedValue(double clock) {
        double sample = input.getGeneratedValue(clock);
        double absoluteSample = Math.abs(sample);

        double targetLevel = level.getGeneratedValue(clock);
        double targetLimit = limit.getGeneratedValue(clock);

        double result;

        if (absoluteSample <= targetLevel) {
            result = absoluteSample;
        } else {
            double limitValue = targetLimit - targetLevel;
            double sampleOverflow = absoluteSample - targetLevel;
            result = targetLevel + limitValue - limitValue / ((sampleOverflow + limitValue) / limitValue);
        }

        return sample >= 0 ? result : -result;
    }
}
