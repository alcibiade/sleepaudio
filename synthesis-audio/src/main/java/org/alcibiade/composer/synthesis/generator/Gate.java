package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * Shut down the signal after a given duration.
 */
@Builder
@Getter
@Setter
@ToString
public class Gate implements Generator {

    private Generator input;
    private Generator duration;

    @Override
    public double getGeneratedValue(double clock) {
        double v = 0;

        if (clock < duration.getGeneratedValue(clock)) {
            v = input.getGeneratedValue(clock);
        }

        return v;
    }
}
