package org.alcibiade.composer.synthesis.sampling;

import org.alcibiade.composer.synthesis.generator.Generator;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.InputStream;

/**
 * Generate totalSamples based on a generator.
 */
public class Sampler implements AudioStreamProvider {

    private Generator generator;
    private long totalSamples;

    private int frequency = 44100;
    private int bits = 16;
    private int channels = 1;
    private AudioFormat audioFormat;

    public Sampler(Generator generator, double durationSeconds) {
        this.generator = generator;
        this.totalSamples = (long) (durationSeconds * frequency);
        this.audioFormat = new AudioFormat(frequency, bits, channels, true, true);
    }

    public AudioFormat getAudioFormat() {
        return audioFormat;
    }

    @Override
    public AudioInputStream getAudioStream() {
        InputStream stream = new ThreadedStreamAdapter(frequency, bits, generator, totalSamples);
        AudioInputStream audioStream = new AudioInputStream(stream, audioFormat, totalSamples);
        return audioStream;
    }

}
