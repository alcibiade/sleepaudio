package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * A fixed value stream.
 */
@Builder
@Getter
@Setter
@ToString
public class FixedValue implements Generator {

    private double value;

    @Override
    public double getGeneratedValue(double clock) {
        return value;
    }
}
