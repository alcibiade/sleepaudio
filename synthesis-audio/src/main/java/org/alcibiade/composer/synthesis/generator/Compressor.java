package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * A dynamic compression component.
 */
@Builder
@Getter
@Setter
@ToString
public class Compressor implements Generator {
    public static final double STRENGTH = 3.;
    private Generator input;
    private Generator level;

    @Override
    public double getGeneratedValue(double clock) {
        double sample = input.getGeneratedValue(clock);
        double targetLevel = level.getGeneratedValue(clock);
        double halfOutput = targetLevel / STRENGTH;

        double result = sample >= 0
                ? targetLevel - targetLevel / ((sample + halfOutput) / halfOutput)
                : -(targetLevel - targetLevel / ((-sample + halfOutput) / halfOutput));

        return result;
    }
}
