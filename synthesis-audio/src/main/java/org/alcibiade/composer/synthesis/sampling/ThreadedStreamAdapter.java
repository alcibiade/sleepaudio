package org.alcibiade.composer.synthesis.sampling;

import org.alcibiade.composer.synthesis.generator.Generator;
import org.alcibiade.composer.synthesis.thread.ChunkValues;
import org.alcibiade.composer.synthesis.thread.SamplingChunkTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Wrap sample encoding to provide a direct input stream for PCM samples.
 */
public class ThreadedStreamAdapter extends InputStream {

    private Logger logger = LoggerFactory.getLogger(ThreadedStreamAdapter.class);

    private long sampleIndex = 0;

    private Deque<Byte> buffer = new LinkedList<>();
    private int frequency;
    private Generator generator;
    private SampleEncoder sampleEncoder;
    private long tsStart;

    private ExecutorService threadPool;
    private List<Future<ChunkValues>> chunks = new ArrayList<>();

    public ThreadedStreamAdapter(int frequency, int bits, Generator generator, long totalSamples) {
        this.frequency = frequency;
        this.generator = generator;
        this.sampleEncoder = new SampleEncoder(bits);

        this.threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        for (long sample = 0; sample < totalSamples; sample += frequency) {
            SamplingChunkTask task = new SamplingChunkTask(sample, frequency, frequency, generator);
            Future<ChunkValues> values = threadPool.submit(task);
            chunks.add(values);
        }

        this.tsStart = System.currentTimeMillis();
    }

    @Override
    public void close() throws IOException {
        logger.debug("Closing stream and assicited thread pool");
        super.close();
        threadPool.shutdown();
        chunks.clear();
    }

    @Override
    public int read() throws IOException {
        if (buffer.isEmpty()) {
            int chunkNumber = (int) (sampleIndex / frequency);
            int chunkOffset = (int) (sampleIndex % frequency);

            try {
                double value = chunks.get(chunkNumber).get().getValues().get(chunkOffset);
                sampleEncoder.encode(value, buffer);
            } catch (InterruptedException | ExecutionException e) {
                throw new IllegalStateException("Chunk rendering failed at sample " + sampleIndex, e);
            }

            if (sampleIndex % frequency == 0) {
                long renderTimeMs = 1000 * sampleIndex / frequency;
                long actualTimeMs = System.currentTimeMillis() - tsStart;

                logger.info("Sampling... {}s done (rendered/elapsed: {}%)",
                        renderTimeMs / 1000,
                        (int) (100. * renderTimeMs / actualTimeMs));
            }

            this.sampleIndex += 1;
        }

        int result = buffer.removeFirst();

        if (result < 0) {
            result += 256;
        }

        return result;
    }
}
