package org.alcibiade.composer.synthesis.generator;

/**
 * Synthesis generator behavior. A single public method means that it can be replaced by a Lambda expression.
 */
public interface Generator {

    double getGeneratedValue(double clock);
}
