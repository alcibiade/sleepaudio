package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.PostConstruct;

/**
 * A generic convolution component. This is a base in reverb and effects implementation.
 */
@Builder
@Getter
@Setter
@ToString
public class PlateReverb implements Generator {

    private Generator input;
    private Generator duration;
    private Convolution convolution;

    @PostConstruct
    public void initializeConvolution() {
        Convolution convolution = Convolution.builder()
            .input(this.input)
            .duration(duration)
            .resolution(t -> 0.05)
            .shape(t -> (t < 0) ? 1. : 0.)
            .build();

        this.convolution = convolution;
    }

    @Override
    public double getGeneratedValue(double clock) {
        return convolution.getGeneratedValue(clock);
    }
}
