package org.alcibiade.composer.synthesis.instrument;

import org.alcibiade.composer.synthesis.generator.*;
import org.springframework.stereotype.Component;

/**
 * Synthetic piano.
 */
@Component
public class Piano implements SynthesisInstrument {

    @Override
    public Generator playNote(double pitch, double duration, double amplitude) {
        Oscillator oscMain = Oscillator.builder().build();
        oscMain.setFrequency(t -> pitch);
        oscMain.setAmplitude(t -> amplitude * 0.75);

        Oscillator oscHarm1 = Oscillator.builder().build();
        oscHarm1.setFrequency(t -> pitch * 0.5);
        oscHarm1.setAmplitude(t -> amplitude * 0.15);

        Oscillator oscHarm2 = Oscillator.builder().build();
        oscHarm2.setFrequency(t -> pitch * 4);
        oscHarm2.setAmplitude(t -> amplitude * 0.10);

        Mix rawSignal = Mix.builder().build();
        rawSignal.setInputs(oscMain, oscHarm1, oscHarm2);

        Adsr env = Adsr.builder().build();
        env.setAttackDuration(t -> 0.02);
        env.setDecayDuration(t -> 0.3);
        env.setSustainDuration(t -> duration);
        env.setSustainLevel(t -> 0.3);
        env.setReleaseDuration(t -> 0.5);

        Amplify amp = Amplify.builder().envelope(env).input(rawSignal).build();

        return amp;
    }
}
