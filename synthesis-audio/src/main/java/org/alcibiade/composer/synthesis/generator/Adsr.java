package org.alcibiade.composer.synthesis.generator;

import lombok.*;
import org.springframework.beans.factory.annotation.Required;

/**
 * Simple ADSR envelope.
 */
@Builder
@Getter
@Setter
@ToString
public class Adsr implements Generator {

    private Generator attackDuration;
    private Generator decayDuration;
    private Generator sustainDuration;
    private Generator sustainLevel;
    private Generator releaseDuration;
    @Builder.Default
    private Generator shapeFactor = t -> 1.;

    private static double interpolate(double fromClock, double toClock, double fromValue, double toValue, double clock, double factor) {
        double ratio = (clock - fromClock) / (toClock - fromClock);
        ratio = Math.pow(ratio, factor);
        return ratio * (toValue - fromValue) + fromValue;
    }

    @Override
    public double getGeneratedValue(double clock) {
        double attackDurationValue = attackDuration.getGeneratedValue(clock);
        double decayDurationValue = decayDuration.getGeneratedValue(clock);
        double sustainDurationValue = sustainDuration.getGeneratedValue(clock);
        double releaseDurationValue = releaseDuration.getGeneratedValue(clock);
        double shapeFactorValue = shapeFactor.getGeneratedValue(clock);

        double sustailLevelValue = sustainLevel.getGeneratedValue(clock);

        double phaseTime = clock;

        if (phaseTime < attackDurationValue) {
            return interpolate(0., attackDurationValue, 0., 1., phaseTime, shapeFactorValue);
        } else {
            phaseTime -= attackDurationValue;
        }

        if (phaseTime < decayDurationValue) {
            return interpolate(decayDurationValue, 0, sustailLevelValue, 1., phaseTime, shapeFactorValue);
        } else {
            phaseTime -= decayDurationValue;
        }

        if (phaseTime < sustainDurationValue) {
            return sustailLevelValue;
        } else {
            phaseTime -= sustainDurationValue;
        }

        if (phaseTime < releaseDurationValue) {
            return interpolate(0., releaseDurationValue, sustailLevelValue, 0., phaseTime, 1);
        }

        return 0;
    }
}
