package org.alcibiade.composer.synthesis.sampling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Deque;

/**
 * Encode values as samples depending on the format.
 */
public class SampleEncoder {
    private Logger logger = LoggerFactory.getLogger(SampleEncoder.class);

    private int bits;
    private boolean clipWarning = false;

    public SampleEncoder(int bits) {
        this.bits = bits;
    }

    public void encode(double value, Deque<Byte> buffer) {
        long sample = (long) value;

        if (!clipWarning && (sample > 0x7FFF || sample < -0x8000)) {
            logger.warn("Clipping for sample value {}", value);
            clipWarning = true;
        }

        switch (bits) {
            case 24:
                buffer.addLast((byte) ((sample & 0xFF0000) >> 16));
                buffer.addLast((byte) ((sample & 0x00FF00) >> 8));
                buffer.addLast((byte) (sample & 0x0000FF));
                break;
            case 16:
                buffer.addLast((byte) ((sample & 0xFF00) >> 8));
                buffer.addLast((byte) (sample & 0xFF));
                break;
            case 8:
                buffer.addLast((byte) ((sample >> 8) & 0xFF));
                break;
            default:
                throw new IllegalStateException("Unsupported sample size " + bits);
        }
    }
}
