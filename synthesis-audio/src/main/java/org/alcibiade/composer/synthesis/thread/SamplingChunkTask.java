package org.alcibiade.composer.synthesis.thread;

import org.alcibiade.composer.synthesis.generator.Generator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Render samples for a chunk.
 */
public class SamplingChunkTask implements Callable<ChunkValues> {
    private Logger logger = LoggerFactory.getLogger(SamplingChunkTask.class);

    private long offset;
    private long sampleCount;
    private int frequency;
    private Generator generator;

    public SamplingChunkTask(long offset, long sampleCount, int frequency, Generator generator) {
        this.offset = offset;
        this.sampleCount = sampleCount;
        this.frequency = frequency;
        this.generator = generator;
    }

    @Override
    public ChunkValues call() {
        long tsStart = System.currentTimeMillis();
        List<Double> values = new ArrayList<>();

        for (long s = 0; s < sampleCount; s++) {
            double clock = (double) (offset + s) / frequency;
            double value = generator.getGeneratedValue(clock);
            values.add(value);
        }

        long tsEnd = System.currentTimeMillis();

        logger.debug("Generated {} values in {}s", values.size(), (tsEnd - tsStart) / 1000.);

        return new ChunkValues(values);
    }
}
