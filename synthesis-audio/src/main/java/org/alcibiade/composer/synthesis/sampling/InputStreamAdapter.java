package org.alcibiade.composer.synthesis.sampling;

import org.alcibiade.composer.synthesis.generator.Generator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Wrap sample encoding to provide a direct input stream for PCM samples.
 */
public class InputStreamAdapter extends InputStream {

    private Logger logger = LoggerFactory.getLogger(InputStreamAdapter.class);

    private long sampleIndex = 0;

    private Deque<Byte> buffer = new LinkedList<>();
    private int frequency;
    private Generator generator;
    private SampleEncoder sampleEncoder;

    public InputStreamAdapter(int frequency, int bits, Generator generator) {
        this.frequency = frequency;
        this.generator = generator;
        this.sampleEncoder = new SampleEncoder(bits);
    }

    @Override
    public int read() throws IOException {
        if (buffer.isEmpty()) {
            double clock = (double) sampleIndex / frequency;
            double value = generator.getGeneratedValue(clock);

            sampleEncoder.encode(value, buffer);

            if (sampleIndex % (frequency / 10) == 0) {
                logger.info("Sampling... {}s done", (double) sampleIndex / frequency);
            }

            this.sampleIndex += 1;
        }

        int result = buffer.removeFirst();

        if (result < 0) {
            result += 256;
        }

        return result;
    }
}
