package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * A simple cold oscillator.
 */
@Builder
@Getter
@Setter
@ToString
public class Oscillator implements Generator {

    private Generator frequency;
    private Generator amplitude;
    @Builder.Default
    private Waveform waveform = Waveform.SINE;

    @Override
    public double getGeneratedValue(double clock) {
        double amplitudeValue = amplitude.getGeneratedValue(clock);
        double frequencyValue = frequency.getGeneratedValue(clock);

        double period = 1. / frequencyValue;
        double phase = clock;

        while (phase > period) {
            phase -= period;
        }

        double phaseRad = 2 * Math.PI * phase / period;
        double result = switch (waveform) {
            case SAWTOOTH -> throw new IllegalStateException("Sawtooth form not available yet.");
            case SINE -> amplitudeValue * Math.sin(phaseRad);
            case SQUARE -> (phaseRad < Math.PI) ? amplitudeValue : -amplitudeValue;
        };

        return result;
    }

    public enum Waveform {
        SINE, SQUARE, SAWTOOTH
    }

}
