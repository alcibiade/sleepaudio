package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.Collection;

/**
 * Amplify a signal based on another one.
 */
@Builder
@Getter
@Setter
@ToString
public class Mix implements Generator {

    private Collection<Generator> inputs;

    public void setInputs(Generator... inputs) {
        this.inputs = Arrays.asList(inputs);
    }

    @Override
    public double getGeneratedValue(double clock) {
        double v = 0;

        for (Generator generator : inputs) {
            v += generator.getGeneratedValue(clock);
        }

        return v;
    }
}
