package org.alcibiade.composer.synthesis.instrument;

import org.alcibiade.composer.synthesis.generator.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Synthetic bell .
 */
@Component
public class Bell implements SynthesisInstrument {

    private List<Double> partialFrequencies;

    public Bell() {
        partialFrequencies = new ArrayList<>();
        partialFrequencies.add(1.0);
        partialFrequencies.add(0.56);
        partialFrequencies.add(0.92);
        partialFrequencies.add(1.19);
        partialFrequencies.add(1.71);
        partialFrequencies.add(2.00);
        partialFrequencies.add(2.74);
        partialFrequencies.add(3.00);
        partialFrequencies.add(3.76);
        partialFrequencies.add(4.07);
    }

    @Override
    public Generator playNote(double pitch, double duration, double amplitude) {

        double a = amplitude;

        List<Generator> frequencies = new ArrayList<>();

        for (double factor : partialFrequencies) {
            Oscillator osc = Oscillator.builder().build();
            a *= 0.50;
            final double localamp = a;
            osc.setAmplitude(t -> localamp);
            osc.setFrequency(t -> pitch * factor);
            osc.setWaveform(Oscillator.Waveform.SINE);

            frequencies.add(osc);
        }

        Mix partials = Mix.builder().inputs(frequencies).build();

        Adsr env = Adsr.builder().build();
        env.setAttackDuration(t -> 0.05);
        env.setDecayDuration(t -> duration);
        env.setSustainDuration(t -> 0.0);
        env.setSustainLevel(t -> 0.0);
        env.setReleaseDuration(t -> 0.0);
        env.setShapeFactor(t -> 2.0);

        Amplify amp = Amplify.builder().envelope(env).input(partials).build();

        return amp;
    }
}
