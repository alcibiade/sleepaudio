package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * Limit output level.
 */
@Builder
@Getter
@Setter
@ToString
public class Shift implements Generator {

    private Generator input;
    private Generator delta;

    @Override
    public double getGeneratedValue(double clock) {
        double inputValue = input.getGeneratedValue(clock);
        double deltaValue = delta.getGeneratedValue(clock);
        return inputValue + deltaValue;
    }
}
