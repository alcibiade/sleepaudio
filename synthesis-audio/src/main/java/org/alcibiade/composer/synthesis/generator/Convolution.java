package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * A generic convolution component. This is a base in reverb and effects implementation.
 */
@Builder
@Getter
@Setter
@ToString
public class Convolution implements Generator {

    private Generator input;
    private Generator shape;
    private Generator duration;
    private Generator resolution;

    @Override
    public double getGeneratedValue(double clock) {
        double duration = this.duration.getGeneratedValue(clock);
        double resolution = this.resolution.getGeneratedValue(clock);

        double result = 0;

        for (double d = -duration; d < duration; d += resolution) {
            result += input.getGeneratedValue(clock + d) * shape.getGeneratedValue(d) * resolution;
        }

        return result;
    }
}
