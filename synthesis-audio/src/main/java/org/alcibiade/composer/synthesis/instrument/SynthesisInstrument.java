package org.alcibiade.composer.synthesis.instrument;

import org.alcibiade.composer.synthesis.generator.Generator;

/**
 * Synthesis instrument.
 */
public interface SynthesisInstrument {

    Generator playNote(double pitch, double duration, double amplitude);
}
