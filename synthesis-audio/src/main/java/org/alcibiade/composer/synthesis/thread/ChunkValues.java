package org.alcibiade.composer.synthesis.thread;

import java.util.List;

/**
 * Level values for a single chunk.
 */
public class ChunkValues {
    private List<Double> values;

    public ChunkValues(List<Double> values) {
        this.values = values;
    }

    public List<Double> getValues() {
        return values;
    }
}
