package org.alcibiade.composer.synthesis.generator;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Required;

/**
 * Amplify a signal based on another one.
 */
@Builder
@Getter
@Setter
@ToString
public class Amplify implements Generator {

    private Generator input;
    private Generator envelope;

    @Override
    public double getGeneratedValue(double clock) {
        return input.getGeneratedValue(clock) * envelope.getGeneratedValue(clock);
    }
}
