package org.alcibiade.composer.synthesis.sampling;

import javax.sound.sampled.AudioInputStream;

/**
 * Provider that acs as a stream factory.
 */
public interface AudioStreamProvider {
    AudioInputStream getAudioStream();
}
