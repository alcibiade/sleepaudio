package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Fixed value generator test.
 */
public class FixedValueTest {

    @Test
    public void testPositiveValue() {
        double value = 174252.4;
        FixedValue generator = FixedValue.builder().value(value).build();

        for (double clock = 0; clock < 100; clock += 0.123) {
            Assertions.assertThat(generator.getGeneratedValue(clock)).isEqualTo(value);
        }
    }

    @Test
    public void testNegativeValue() {
        double value = -12397.1;
        FixedValue generator = FixedValue.builder().value(value).build();

        for (double clock = 0; clock < 100; clock += 0.123) {
            Assertions.assertThat(generator.getGeneratedValue(clock)).isEqualTo(value);
        }
    }
}
