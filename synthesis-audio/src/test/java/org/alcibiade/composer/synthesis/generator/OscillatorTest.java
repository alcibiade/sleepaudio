package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.Test;

/**
 * Fixed value generator test.
 */
public class OscillatorTest {
    private static final double MARGIN = 0.0001;

    @Test
    public void testSine() {
        FixedValue freq = FixedValue.builder().value(440.).build();
        FixedValue amp = FixedValue.builder().value(10000).build();


        Oscillator oscillator = Oscillator.builder().build();
        oscillator.setFrequency(freq);
        oscillator.setAmplitude(amp);
        oscillator.setWaveform(Oscillator.Waveform.SINE);

        for (int i = 0; i < 10; i++) {
            Assertions.assertThat(oscillator.getGeneratedValue(i / 440.)).isCloseTo(0., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.5) / 440.)).isCloseTo(0., Offset.offset(MARGIN));

            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.25) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.75) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
        }
    }

    @Test
    public void testSquare() {
        FixedValue freq = FixedValue.builder().value(440.).build();
        FixedValue amp = FixedValue.builder().value(10000).build();

        Oscillator oscillator = Oscillator.builder().build();
        oscillator.setFrequency(freq);
        oscillator.setAmplitude(amp);
        oscillator.setWaveform(Oscillator.Waveform.SQUARE);

        for (int i = 0; i < 10; i++) {
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.01) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.11) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.21) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.31) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.41) / 440.)).isCloseTo(10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.51) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.61) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.71) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.81) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
            Assertions.assertThat(oscillator.getGeneratedValue((i + 0.91) / 440.)).isCloseTo(-10000., Offset.offset(MARGIN));
        }
    }
}
