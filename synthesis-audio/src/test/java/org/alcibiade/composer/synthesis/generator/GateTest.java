package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Fixed value generator test.
 */
public class GateTest {

    @Test
    public void testGate() {
        double value = 174252.4;
        FixedValue generator = FixedValue.builder().value(value).build();
        Gate gate = Gate.builder().input(generator).duration(t -> 20).build();

        for (double clock = 0; clock < 100; clock += 0.123) {
            if (clock < 20) {
                Assertions.assertThat(gate.getGeneratedValue(clock)).isEqualTo(value);
            } else {
                Assertions.assertThat(gate.getGeneratedValue(clock)).isEqualTo(0);
            }
        }
    }

}
