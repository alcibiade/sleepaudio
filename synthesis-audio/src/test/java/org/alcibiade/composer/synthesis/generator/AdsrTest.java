package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.Test;

/**
 * Fixed value generator test.
 */
public class AdsrTest {
    private static final double MARGIN = 0.0001;

    @Test
    public void testAdsr() {
        Adsr adsr = Adsr.builder().build();
        adsr.setAttackDuration(t -> 0.5);
        adsr.setDecayDuration(t -> 0.4);
        adsr.setSustainDuration(t -> 3.1);
        adsr.setSustainLevel(t -> 0.7);
        adsr.setReleaseDuration(t -> 1.0);

        Assertions.assertThat(adsr.getGeneratedValue(0.00)).isCloseTo(0.00, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(0.25)).isCloseTo(0.50, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(0.50)).isCloseTo(1.00, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(0.70)).isCloseTo(0.85, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(0.90)).isCloseTo(0.70, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(1.00)).isCloseTo(0.70, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(2.00)).isCloseTo(0.70, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(3.00)).isCloseTo(0.70, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(4.00)).isCloseTo(0.70, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(4.50)).isCloseTo(0.35, Offset.offset(MARGIN));
        Assertions.assertThat(adsr.getGeneratedValue(5.00)).isCloseTo(0.00, Offset.offset(MARGIN));
    }

    @Test
    public void testBellEnvelope() {
        Adsr adsr = Adsr.builder().build();
        adsr.setAttackDuration(t -> 0.003);
        adsr.setDecayDuration(t -> 6);
        adsr.setSustainDuration(t -> 0);
        adsr.setSustainLevel(t -> 0);
        adsr.setReleaseDuration(t -> 0);

        Assertions.assertThat(adsr.getGeneratedValue(0.000)).isCloseTo(0.00, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.003)).isCloseTo(1.00, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(3.002)).isCloseTo(0.50, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(6.003)).isCloseTo(0.00, Offset.offset(0.01));
    }

    @Test
    public void testDecayFactor() {
        Adsr adsr = Adsr.builder().build();
        adsr.setAttackDuration(t -> 0.5);
        adsr.setDecayDuration(t -> 0.5);
        adsr.setSustainDuration(t -> 0);
        adsr.setSustainLevel(t -> 0);
        adsr.setReleaseDuration(t -> 0);
        adsr.setShapeFactor(t -> 2.);

        Assertions.assertThat(adsr.getGeneratedValue(0.000)).isCloseTo(0.00, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.100)).isCloseTo(0.04, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.200)).isCloseTo(0.16, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.300)).isCloseTo(0.36, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.400)).isCloseTo(0.64, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.500)).isCloseTo(1.00, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.600)).isCloseTo(0.64, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.700)).isCloseTo(0.36, Offset.offset(0.01));
        Assertions.assertThat(adsr.getGeneratedValue(0.800)).isCloseTo(0.16, Offset.offset(0.01));
    }
}
