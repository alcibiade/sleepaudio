package org.alcibiade.composer.synthesis.sampling;

import org.alcibiade.composer.synthesis.generator.Oscillator;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Global stream adapters tests.
 */
public class StreamAdaptersTest {

    @Test
    public void testStreamComparison() throws IOException {
        Oscillator generator = Oscillator.builder().build();
        generator.setAmplitude(t -> 10000);
        generator.setFrequency(t -> 440);
        generator.setWaveform(Oscillator.Waveform.SINE);

        int totalSamples = 10 * 44100;

        InputStream sequentialAdapter = new InputStreamAdapter(44100, 8, generator);
        InputStream threadedAdapter = new ThreadedStreamAdapter(44100, 8, generator, totalSamples);

        for (int s = 0; s < totalSamples; s++) {
            int vs = sequentialAdapter.read();
            int ts = threadedAdapter.read();

            Assertions.assertThat(vs).describedAs("Sample at offset %d", s).isEqualTo(ts);
        }
    }
}
