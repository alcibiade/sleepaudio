package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.Test;

/**
 * Test compression.
 */
public class LimiterTest {

    @Test
    public void testLimiter() {
        Oscillator osc = Oscillator.builder().amplitude(t -> 40000).frequency(t -> 80).build();
        Limiter limiter = Limiter.builder().input(osc).level(t -> 50000).limit(t -> 60000).build();

        for (double t = 0; t < 2.; t += 0.02) {
            double oscValue = osc.getGeneratedValue(t);
            Assertions.assertThat(limiter.getGeneratedValue(t))
                .isCloseTo(oscValue, Offset.offset(0.0001));
        }

        limiter.setLevel(t -> 20000);
        limiter.setLimit(t -> 30000);

        for (double t = 0; t < 2.; t += 0.02) {
            double oscValue = osc.getGeneratedValue(t);
            if (Math.abs(oscValue) <= 20000) {
                Assertions.assertThat(limiter.getGeneratedValue(t))
                    .isCloseTo(oscValue, Offset.offset(0.0001));
            } else {
                Assertions.assertThat(Math.abs(limiter.getGeneratedValue(t)))
                    .isGreaterThan(20000)
                    .isLessThan(30001)
                    .isLessThan(Math.abs(oscValue));
            }
        }

        limiter.setLevel(t -> 3000);
        limiter.setLimit(t -> 5000);

        for (double t = 0; t < 2.; t += 0.02) {
            double oscValue = osc.getGeneratedValue(t);
            if (Math.abs(oscValue) <= 3000) {
                Assertions.assertThat(limiter.getGeneratedValue(t))
                    .isCloseTo(oscValue, Offset.offset(0.0001));
            } else {
                Assertions.assertThat(Math.abs(limiter.getGeneratedValue(t)))
                    .isGreaterThan(3000)
                    .isLessThan(5001)
                    .isLessThan(Math.abs(oscValue));
            }
        }
    }
}
