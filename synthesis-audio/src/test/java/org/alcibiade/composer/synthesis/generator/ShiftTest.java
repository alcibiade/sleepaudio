package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.Test;

/**
 * Shift generator test.
 */
public class ShiftTest {

    @Test
    public void testPositiveShift() {
        Oscillator oscillator = Oscillator.builder().build();
        oscillator.setAmplitude(t -> 10000);
        oscillator.setFrequency(t -> 440.);

        Shift shift = Shift.builder().input(oscillator).delta(t -> 4000).build();

        for (double clock = 0; clock < 100; clock += 0.123) {
            Assertions.assertThat(shift.getGeneratedValue(clock))
                .isCloseTo(oscillator.getGeneratedValue(clock) + 4000, Offset.offset(0.001));
        }
    }
}
