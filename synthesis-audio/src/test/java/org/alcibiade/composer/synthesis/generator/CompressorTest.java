package org.alcibiade.composer.synthesis.generator;

import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Test compression.
 */
public class CompressorTest {

    @Test
    public void testCompression() {
        Oscillator osc = Oscillator.builder().amplitude(t -> 40000).frequency(t -> 80).build();
        Compressor compressor = Compressor.builder().input(osc).level(t -> 20000).build();

        for (double t = 0; t < 2.; t += 0.02) {
            Assertions.assertThat(Math.abs(compressor.getGeneratedValue(t))).isLessThan(20001);
        }

        compressor.setLevel(t -> 5000);

        for (double t = 0; t < 2.; t += 0.02) {
            Assertions.assertThat(Math.abs(compressor.getGeneratedValue(t))).isLessThan(5001);
        }
    }
}
