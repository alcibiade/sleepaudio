package org.alcibiade.composer.synthesis.generator;


import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Test convolution.
 */
public class ConvolutionTest {

    @Test
    public void testConvolution() {
        Oscillator osc = Oscillator.builder().build();
        osc.setAmplitude(t -> 30000);
        osc.setFrequency(t -> 80);

        Convolution convolution = Convolution.builder()
            .input(osc)
            .shape(t -> 1)
            .duration(t -> 1.)
            .resolution(t -> 1. / 100)
            .build();

        for (double t = 0; t < 2.; t += 0.02) {
            double output = convolution.getGeneratedValue(t);
            Assertions.assertThat(output).isLessThan(30001);
        }
    }
}
