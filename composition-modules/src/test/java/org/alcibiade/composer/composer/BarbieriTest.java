package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.random.RandomFactory;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Test the Barbieri composer.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class BarbieriTest {

    @Test
    public void testNotesDuration() {
        Piece piece = new Piece("Barbieri solo", new Key(Pitch.A4, Scale.MINOR), 90, new Clock(12));
        RandomFactory randomFactory = new RandomFactory();

        Barbieri composer = new Barbieri(4);
        composer.setRandomFactory(randomFactory);

        Track<Event<Note>> track = composer.composeTrack(piece);

        track.getEvents().stream().forEach(event ->
                Assertions.assertThat(event.getTime().isBeat())
                        .describedAs("Event %s is on the beat", event.toString())
                        .isTrue()
        );
    }

}
