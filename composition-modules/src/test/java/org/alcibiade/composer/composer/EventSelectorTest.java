package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.*;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * Test selector behavior.
 */
public class EventSelectorTest {

    @Test
    public void testRandomSelection() {
        Event<Note> evt1 = new Event<>(new Clock(0.), new Note(Pitch.A4,
                new Clock(1), Velocity.FF));
        Event<Note> evt2 = new Event<>(new Clock(1.), new Note(Pitch.C3,
                new Clock(1), Velocity.FF));
        EventSet<Event<Note>> set = new EventSet<>();
        set.add(evt1);
        set.add(evt2);

        Random random = new Random();
        EventSelector eventSelector = new EventSelector(random, set);

        for (int i = 0; i < 10; i++) {
            Event<Note> evt = eventSelector.getRandomEvent();
            assertTrue(evt == evt1 || evt == evt2);
        }
    }
}
