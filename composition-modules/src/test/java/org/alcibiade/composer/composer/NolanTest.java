package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.random.RandomFactory;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Test the Nolan composer.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class NolanTest {

    @Test
    public void testNotesGeneration() {
        RandomFactory randomFactory = new RandomFactory();
        Piece piece = new Piece("Nolan solo", new Key(Pitch.A4, Scale.MINOR), 90, new Clock(12));
        Nolan composer = new Nolan(4);
        composer.setRandomFactory(randomFactory);
        Track<Event<Note>> track = composer.composeTrack(piece);
        Assertions.assertThat(track.getEvents()).isNotEmpty();
    }

}
