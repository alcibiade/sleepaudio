package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.random.RandomFactory;
import org.assertj.core.api.Assertions;
import org.junit.Test;

/**
 * Test the Bach composer.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class BachTest {

    @Test
    public void testComposition() {
        RandomFactory randomFactory = new RandomFactory();

        Piece piece = new Piece("Bach solo", new Key(Pitch.A4, Scale.MINOR), 90, new Clock(12));
        Bach composer = new Bach(4);
        composer.setRandomFactory(randomFactory);

        Track<Event<Note>> track = composer.composeTrack(piece);

        track.getEvents().stream().forEach(event ->
                Assertions.assertThat(event.getTime().isBeat())
                        .describedAs("Event %s is on the beat", event.toString())
                        .isTrue()
        );
    }

}
