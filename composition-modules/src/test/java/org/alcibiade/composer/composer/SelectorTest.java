package org.alcibiade.composer.composer;

import org.alcibiade.asciiart.widget.model.TableModel;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SelectorTest extends AbstractSelectorTest {

    @Test
    public void testSelector() {
        Selector<String> s = new Selector<>();
        assertEquals(0, s.size());
    }

    @Test
    public void testAdd() {
        Selector<String> s = new Selector<>();
        assertEquals(0, s.size());
        s.add("T1");
        assertEquals(1, s.size());
        s.add("T2");
        assertEquals(2, s.size());
        s.add("T3");
        assertEquals(3, s.size());
    }

    @Test
    public void testWeight() {
        Selector<String> s = new Selector<>();
        assertEquals(0, s.size());
        s.add("T1");
        s.weight("T1", 2.);
        assertEquals(1, s.size());
        s.weight("T2", 2.);
        assertEquals(1, s.size());
        assertFalse(s.contains("T2"));
    }

    @Test
    public void testGet() {
        Selector<String> s = new Selector<>();
        assertEquals(0, s.size());
        s.add("T1");
        s.weight("T1", 1.);
        assertEquals(1, s.size());
        s.add("T2");
        s.weight("T2", 2.);
        assertEquals(2, s.size());

        Map<String, Integer> results = runSelections(s, 10_000);

        assertEquals(2 * results.get("T1"), results.get("T2"), 1_000);
    }

    @Test
    public void testGetTableModel() {
        Selector<String> s = new Selector<>();
        assertEquals(0, s.size());
        s.add("T1");
        assertEquals(1, s.size());
        s.add("T2");
        assertEquals(2, s.size());
        s.add("T3");
        assertEquals(3, s.size());

        TableModel tm = s.getTableModel();
        assertEquals(3, tm.getHeight());
        assertEquals(2, tm.getWidth());
        assertEquals("T2", tm.getCellContent(0, 1));
    }
}
