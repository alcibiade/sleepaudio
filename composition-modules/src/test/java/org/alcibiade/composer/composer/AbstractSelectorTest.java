package org.alcibiade.composer.composer;

import org.alcibiade.composer.generator.Generator;
import org.alcibiade.composer.generator.WhiteGenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AbstractSelectorTest {

    public AbstractSelectorTest() {
        super();
    }

    protected <T> Map<T, Integer> runSelections(Selector<T> s, int iterations) {
        Map<T, Integer> results = new HashMap<>();
        Random random = new Random();

        Generator gen = new WhiteGenerator(random);

        for (int i = 0; i < iterations; i++) {
            T result = s.get(gen, i);
            Integer occurrences = results.get(result);

            if (occurrences == null) {
                occurrences = 0;
            }

            results.put(result, occurrences + 1);
        }

        return results;
    }
}
