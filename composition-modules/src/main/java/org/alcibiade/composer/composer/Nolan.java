package org.alcibiade.composer.composer;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.alcibiade.composer.generator.Generator;
import org.alcibiade.composer.generator.InterpolatingGenerator;
import org.alcibiade.composer.generator.extender.FractalExtender;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.alcibiade.composer.piece.*;

import java.util.Random;
import java.util.Set;

@ToString
@RequiredArgsConstructor
public class Nolan extends AbstractComposer {

    private static final double ARP_STEP = 0.5;
    private final double arpPercent;

    @Override
    public Track<Event<Note>> composeTrack(Piece piece) {
        Track<Event<Note>> mytrack = new Track<>("Nolan's arpeggios", Instrument.SYNTH_LEAD);

        Random random = randomFactory.getRandom();

        Generator enablerGen = new InterpolatingGenerator(0, 1,
            new LinearInterpolator(), new FractalExtender(random, 8, 4, 4));

        Generator pitchGen = new InterpolatingGenerator(-8, 10,
            new LinearInterpolator(), new FractalExtender(random, 24 * ARP_STEP, 4, 2));

        Clock position = Clock.ZERO;
        Pitch lastpitch = null;

        while (position.compareTo(piece.getDuration()) < 0) {
            Clock duration = new Clock(ARP_STEP);
            double enabler = enablerGen.get(position.getAsDouble());

            if (enabler < arpPercent) {
                if (enabler < arpPercent / 2.) {
                    duration = new Clock(duration.getAsDouble() / 2.);
                }

                PitchSelector selector = new PitchSelector(piece.getKey(),
                    mytrack.getInstrument().getRange());

                Set<Pitch> pitches = piece.getEvents(position,
                    position.add(duration)).getPitches();

                selector.weightPitches(pitches, 2.0);

                if (lastpitch != null) {
                    selector.weightPitch(lastpitch, 0.0);
                }

                Pitch pitch = selector.get(pitchGen, position.getAsDouble());
                lastpitch = pitch;

                Event<Note> event = new Event<>(position, new Note(pitch,
                    duration, new Velocity(Velocity.DYNAMIC_MF,
                    getBalance())));
                mytrack.addEvent(event);
            }

            position = position.add(duration);
        }

        return mytrack;
    }
}
