package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.Velocity;
import org.alcibiade.composer.random.RandomFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

public abstract class AbstractComposer implements Composer {

    public static final double RANDOM_FACTOR = 0.25;
    @Autowired
    RandomFactory randomFactory;
    private Integer balance = null;

    public void setRandomFactory(RandomFactory randomFactory) {
        this.randomFactory = randomFactory;
    }

    public synchronized int getBalance() {
        if (balance == null) {
            Random random = randomFactory.getRandom();

            balance = random.nextInt((int) (Velocity.MAX_VALUE * RANDOM_FACTOR))
                    - (int) (Velocity.MAX_VALUE * RANDOM_FACTOR / 2)
                    + Velocity.MAX_VALUE / 2;
        }

        return balance;
    }
}
