package org.alcibiade.composer.composer;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.alcibiade.composer.generator.Generator;
import org.alcibiade.composer.generator.InterpolatingGenerator;
import org.alcibiade.composer.generator.extender.FractalExtender;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.alcibiade.composer.piece.*;
import org.slf4j.MDC;

import java.util.Random;
import java.util.Set;

@ToString
public class Bach extends AbstractComposer {

    private final Clock timeBase;

    public Bach(double timeBase) {
        this.timeBase = new Clock(timeBase);
    }

    @Override
    public Track<Event<Note>> composeTrack(Piece piece) {
        MDC.put("composer", "Bach");
        Track<Event<Note>> mytrack = new Track<>("Bach", Instrument.PIANO);

        Random random = randomFactory.getRandom();
        Generator genPitch = new InterpolatingGenerator(-10, 15,
            new LinearInterpolator(), new FractalExtender(random, 4., 4, 1.0));

        Clock position = Clock.ZERO;
        Pitch lastPitch = null;
        PitchHistory pitchHistory = new PitchHistory(12);

        while (position.compareTo(piece.getDuration()) < 0) {
            MDC.put("position", position.toString());
            // Allow note repetition if we started a new bar.
            if (position.isBar()) {
                lastPitch = null;
            }

            // This selector will be used to compute the pitch of the next note.
            PitchSelector selector = new PitchSelector(
                piece.getKey(), mytrack.getInstrument().getRange());

            // Get pitches of other tracks.
            Set<Pitch> otherPitches = piece.getEvents(position,
                position.add(timeBase)).getPitches();

            // Increase the probability of occurence of these pitches. If a
            // pitch is not in the current key, it won't be selected anyway
            // as it has a weight of 0 in the selector.
            selector.weightPitches(otherPitches, 1.4);

            // Increase probablity of pitches close to the last played one.
            if (lastPitch != null) {
                for (int halfTones = -4; halfTones < 5; halfTones++) {
                    Interval interval = Interval.builder().halfTones(halfTones).build();
                    Pitch p = lastPitch.add(interval);
                    selector.weightPitch(p, 2);
                }
            }

            // Decrease probablity of pitches recently played.
            pitchHistory.stream().forEach(entry
                -> selector.weightPitch(
                entry.getPitch(),
                Math.pow(0.2, -entry.getAge())));

            // Actually select the pitch.
            Pitch pitch = selector.get(genPitch, position.getAsDouble());

            // Consecutive notes are not played and are rather replaced by silence.
            if (!pitch.equals(lastPitch)) {
                Event<Note> event = new Event<>(position, new Note(pitch,
                    timeBase, new Velocity(Velocity.DYNAMIC_MF,
                    getBalance())));

                mytrack.addEvent(event);
            }

            lastPitch = pitch;
            position = position.add(timeBase);
        }

        MDC.remove("position");
        MDC.remove("composer");
        return mytrack;
    }
}
