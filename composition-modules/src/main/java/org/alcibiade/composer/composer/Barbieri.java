package org.alcibiade.composer.composer;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.alcibiade.asciiart.widget.TableWidget;
import org.alcibiade.asciiart.widget.TextPanel;
import org.alcibiade.asciiart.widget.layout.GridLayout;
import org.alcibiade.asciiart.widget.model.TableModel;
import org.alcibiade.asciiart.widget.model.TableModelCollectionAdapter;
import org.alcibiade.composer.generator.Generator;
import org.alcibiade.composer.generator.InterpolatingGenerator;
import org.alcibiade.composer.generator.extender.FractalExtender;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.alcibiade.composer.piece.*;
import org.slf4j.MDC;

import java.util.Random;
import java.util.Set;

@ToString
@RequiredArgsConstructor
public class Barbieri extends AbstractComposer {

    private static final int OVERLAP_TIME = 1;

    private final double timeBase;

    @Override
    public Track<Event<Note>> composeTrack(Piece piece) {
        MDC.put("composer", "Barbieri");
        Instrument instrument = Instrument.PAD;
        Track<Event<Note>> mytrack = new Track<>("Barbieri's pad", instrument);
        Random random = randomFactory.getRandom();

        Generator genPitch = new InterpolatingGenerator(-10, 15,
                new LinearInterpolator(), new FractalExtender(random, 4., 4, 2));

        Generator genDuration = new InterpolatingGenerator(1, 5,
                new LinearInterpolator(), new FractalExtender(random, 4., 4, 2));

        Clock position = Clock.ZERO;

        while (position.compareTo(piece.getDuration()) < 0) {
            MDC.put("position", position.toString());
            double d = genDuration.get(position.getAsDouble());
            Clock duration = new Clock(timeBase + timeBase * ((int) d));

            PitchSelector selector = new PitchSelector(piece.getKey(), mytrack.getInstrument().getRange());

            Set<Pitch> myPitches = piece.getEvents(position,
                    position.add(duration), instrument, true).getPitches();

            Set<Pitch> otherPitches = piece.getEvents(position,
                    position.add(duration), instrument, false).getPitches();

            selector.weightPitches(myPitches, 0., 0.0);
            selector.weightPitches(otherPitches, 3.0, 0.0, 2.0);

            Pitch pitch = selector.get(genPitch, position.getAsDouble());

            Velocity velocity = new Velocity(Velocity.DYNAMIC_MF, getBalance());
            Clock overlapTime = new Clock(OVERLAP_TIME);
            Note note = new Note(pitch, duration.add(overlapTime), velocity);
            Event<Note> event = new Event<>(position, note);

            mytrack.addEvent(event);

            position = position.add(duration);

            // Construct comment
            TextPanel panel = new TextPanel(new GridLayout(2));
            TableModel tableModel = selector.getTableModel();
            panel.add(new TableWidget(tableModel));

            TextPanel pitchesPanel = new TextPanel();
            pitchesPanel.add(new TableWidget(new TableModelCollectionAdapter(
                    myPitches, "My Pitches")));
            pitchesPanel.add(new TableWidget(new TableModelCollectionAdapter(
                    otherPitches, "Other Pitches")));
            panel.add(pitchesPanel);

            event.setComment(panel);
        }

        MDC.remove("position");
        MDC.remove("composer");

        return mytrack;
    }
}
