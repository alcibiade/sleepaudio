package org.alcibiade.composer.composer;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.random.RandomFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

@ToString
@RequiredArgsConstructor
public class Accentuator extends AbstractComposer {

    private final double intervalBase;

    @Autowired
    private RandomFactory randomFactory;

    @Override
    public Track<Event<Note>> composeTrack(Piece piece) {
        Instrument instrument = Instrument.PIANO;
        Track<Event<Note>> mytrack = new Track<>("Accents", instrument);
        Random random = randomFactory.getRandom();

        Clock position = Clock.ZERO;

        while (position.compareTo(piece.getDuration()) < 0) {
            Clock duration = new Clock(intervalBase);

            EventSet<Event<Note>> notes = piece.getEvents(position, position.add(duration));

            EventSelector selector = new EventSelector(random, notes);
            Event<Note> event = selector.getRandomEvent();
            Pitch eventPitch = event.getItem().getPitch();
            Pitch myPitch = instrument.getRange().transpose(eventPitch);
            Note note = new Note(myPitch, duration, new Velocity(
                    Velocity.DYNAMIC_MP, getBalance()));
            mytrack.addEvent(new Event<>(event.getTime(), note));

            position = position.add(duration);
        }

        return mytrack;
    }
}
