package org.alcibiade.composer.composer;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.alcibiade.asciiart.widget.model.TableModel;
import org.alcibiade.asciiart.widget.model.TableModelMapAdapter;
import org.alcibiade.composer.generator.Generator;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

@ToString
@RequiredArgsConstructor
public class Selector<T> {

    private static final double BASE_WEIGHT = 1000.;
    private final Map<T, Double> candidates = new TreeMap<>();

    public int size() {
        return candidates.size();
    }

    public void add(T item) {
        candidates.put(item, BASE_WEIGHT);
    }

    public void weight(T item, double ratio) {
        if (candidates.containsKey(item)) {
            double currentWeight = candidates.get(item);
            candidates.put(item, currentWeight * ratio);
        }
    }

    public T get(Generator genPitch, double position) {
        double weightTotal = computeWeightSum(candidates);

        double target = (genPitch.get(position) - genPitch.getLowBound())
            * weightTotal
            / (genPitch.getHighBound() - genPitch.getLowBound());

        T pitch = selectCandidate(candidates, target);
        return pitch;
    }

    /**
     * Compute sum of all weights
     *
     * @param candidates The Pitch->Weight map
     * @return The global weight
     */
    private double computeWeightSum(Map<T, Double> candidates) {
        double weightTotal = 0;

        for (Double weight : candidates.values()) {
            weightTotal += weight;
        }

        return weightTotal;
    }

    /**
     * Randomly select a pitch amongst candidates according to their weights.
     *
     * @param candidates The T->Weight map of all candidates.
     * @param target     The target weight 0 <= target <= weightSum
     * @return The selected pitch.
     */
    private T selectCandidate(Map<T, Double> candidates, double target) {
        T pitch = null;
        Iterator<Map.Entry<T, Double>> itPitch = candidates.entrySet().iterator();
        double remaining = target;
        while (itPitch.hasNext() && remaining >= 0.) {
            Map.Entry<T, Double> entry = itPitch.next();
            pitch = entry.getKey();
            remaining -= entry.getValue();
        }

        return pitch;
    }

    /**
     * Get a table model backed by this pitch selector.
     *
     * @return a table model instance
     */
    public TableModel getTableModel() {
        return new TableModelMapAdapter(candidates, "Pitch", "Weight");
    }

    public boolean contains(T item) {
        return candidates.containsKey(item);
    }
}
