package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.Event;
import org.alcibiade.composer.piece.EventSet;
import org.alcibiade.composer.piece.Note;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Select events from a set.
 */
public class EventSelector {
    private Random random;
    private EventSet<Event<Note>> notes;

    public EventSelector(Random random, EventSet<Event<Note>> notes) {
        this.random = random;
        this.notes = notes;
    }

    public Event<Note> getRandomEvent() {
        int randomIndex = random.nextInt(notes.size());
        List<Event<Note>> eventsList = new ArrayList<>(notes);
        Event<Note> randomEvent = eventsList.get(randomIndex);
        return randomEvent;
    }
}
