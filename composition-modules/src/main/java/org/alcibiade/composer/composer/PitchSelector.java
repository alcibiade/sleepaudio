package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.Interval;
import org.alcibiade.composer.piece.Key;
import org.alcibiade.composer.piece.Pitch;
import org.alcibiade.composer.piece.PitchRange;

import java.util.Collection;

public class PitchSelector extends Selector<Pitch> {

    private Key key;
    private PitchRange range;

    public PitchSelector(Key key, PitchRange range) {
        this.key = key;
        this.range = range;
        fillCandidates();
    }

    public void weightPitches(Collection<Pitch> pitches, double ratio) {
        weightPitches(pitches, ratio, 1.);
    }

    public void weightPitches(Collection<Pitch> pitches, double ratio,
                              double neighborRatio) {
        weightPitches(pitches, ratio, neighborRatio, 1.);
    }

    public void weightPitches(Collection<Pitch> pitches, double ratio,
                              double neighborRatio, double fifthRatio) {
        pitches.stream().forEach((pitch) -> {
            weightPitch(pitch, ratio);
            weightPitch(pitch.subtract(Interval.HALF_TONE), neighborRatio);
            weightPitch(pitch.add(Interval.HALF_TONE), neighborRatio);
            weightPitch(pitch.add(Interval.FIFTH), fifthRatio);
        });
    }

    public void weightPitch(Pitch pitch, double ratio) {
        Pitch currentPitch = pitch.transpose(range.getLow());
        while (range.contains(currentPitch)) {
            weight(currentPitch, ratio);
            currentPitch = currentPitch.add(Interval.OCTAVE);
        }
    }

    /**
     * Select pitches according to the key and the range.
     */
    private void fillCandidates() {
        Pitch pitch = range.getLow();

        while (range.contains(pitch)) {
            if (key.contains(pitch)) {
                add(pitch);
            }

            pitch = pitch.add(Interval.HALF_TONE);
        }
    }
}
