package org.alcibiade.composer.composer;

import org.alcibiade.composer.piece.Event;
import org.alcibiade.composer.piece.Note;
import org.alcibiade.composer.piece.Piece;
import org.alcibiade.composer.piece.Track;

/**
 * Global composer behavior interface.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public interface Composer {

    /**
     * Compose a new track fitting to the piece.
     *
     * @param piece the piece to base composition on.
     * @return a new track composed as a potential addition to this piece.
     */
    Track<Event<Note>> composeTrack(Piece piece);
}
