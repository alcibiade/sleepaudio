package org.alcibiade.composer.composer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.alcibiade.composer.piece.Pitch;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Store pitch history to gradually act on selector weight.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class PitchHistory {

    private List<Entry> historyEntries;
    private int size;

    public PitchHistory(int size) {
        this.historyEntries = new LinkedList<>();
        this.size = size;
    }

    public void clear() {
        historyEntries.clear();
    }

    public void add(Pitch pitch) {
        historyEntries.add(new Entry(pitch));

        while (historyEntries.size() > size) {
            historyEntries.remove(0);
        }

        historyEntries.forEach(Entry::increaseAge);
    }

    public Stream<Entry> stream() {
        return historyEntries.stream();
    }

    @RequiredArgsConstructor
    @Getter
    public class Entry {
        private final Pitch pitch;
        private int age = 0;

        public void increaseAge() {
            age += 1;
        }
    }
}
