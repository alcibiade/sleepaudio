package org.alcibiade.composer.video;

import java.awt.image.BufferedImage;

public class BackdropImageGenerator implements VideoGenerator {


    @Override
    public BufferedImage createImage(FrameParameters frameParameters) {
        return new BufferedImage(frameParameters.getWidth(), frameParameters.getHeight(), BufferedImage.TYPE_INT_RGB);
    }
}
