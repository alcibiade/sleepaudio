package org.alcibiade.composer.video;

import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.piece.Event;

import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.Set;

public class PianoRollImageGenerator implements VideoGenerator {

    private Piece piece;

    public PianoRollImageGenerator(Piece piece) {
        this.piece = piece;
    }

    @Override
    public BufferedImage createImage(FrameParameters frameParameters) {
        BufferedImage image = new BufferedImage(
                frameParameters.getWidth(),
                frameParameters.getHeight(),
                BufferedImage.TYPE_INT_RGB
        );

        Graphics2D g2d = image.createGraphics();
        g2d.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        g2d.setColor(new Color(24, 232, 87, 100));
        g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

        g2d.translate(frameParameters.getWidth() / 4, 0);
        g2d.setColor(new Color(200, 255, 200));

        g2d.drawLine(0, 0, 0, frameParameters.getHeight());

        double scaleX = 40 * frameParameters.getWidth() / 1280;
        double barH = 15 * frameParameters.getHeight() / 720;

        g2d.translate(-frameParameters.getTime() * scaleX * piece.getTempo() / 60, 0);

        Clock tFrom = Clock.ZERO;
        Clock tTo = piece.getDuration();

        Set<Track<Event<Note>>> tracks = piece.getNoteTracks();

        tracks.forEach(track -> track.getEvents(tFrom, tTo).forEach(event -> {
            Note note = event.getItem();

            RoundRectangle2D.Double rect = new RoundRectangle2D.Double(
                    scaleX * event.getTime().getAsDouble(),
                    frameParameters.getHeight() - note.getPitch().getFrequency(),
                    scaleX * event.getDuration().getAsDouble(),
                    barH,
                    barH / 3, barH / 3
            );

            g2d.setColor(new Color(24, 232, 87, 30));
            g2d.fill(rect);
            g2d.setColor(new Color(24, 232, 87, 100));
            g2d.draw(rect);
        }));

        return image;
    }
}
