package org.alcibiade.composer.video;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class HaloGenerator implements VideoGenerator {

    private VideoGenerator originalGenerator;

    public HaloGenerator(VideoGenerator originalGenerator) {
        this.originalGenerator = originalGenerator;
    }

    @Override
    public BufferedImage createImage(FrameParameters frameParameters) {
        int haloRadius = frameParameters.getHeight() / 70;

        BufferedImage originalImage = originalGenerator.createImage(frameParameters);

        BufferedImage halo = new BufferedImage(
                frameParameters.getWidth(),
                frameParameters.getHeight(),
                BufferedImage.TYPE_INT_RGB
        );

        for (int y = 0; y < halo.getHeight(); y++) {
            for (int x = 0; x < halo.getWidth(); x++) {
                List<RGB> samples = new ArrayList<>();

                for (int dy = -haloRadius; dy <= haloRadius; dy++) {
                    for (int dx = -haloRadius; dx <= haloRadius; dx++) {
                        int ax = Math.min(Math.max(x + dx, 0), halo.getWidth() - 1);
                        int ay = Math.min(Math.max(y + dy, 0), halo.getHeight() - 1);
                        samples.add(new RGB(originalImage.getRGB(ax, ay)));
                    }
                }

                halo.setRGB(x, y, merge(samples).getRgbValue());
            }
        }

        return halo;
    }

    private RGB merge(List<RGB> samples) {
        int r = 0;
        int g = 0;
        int b = 0;

        for (RGB rgb : samples) {
            r += rgb.getRed();
            g += rgb.getGreen();
            b += rgb.getBlue();
        }

        return new RGB(r / samples.size(), g / samples.size(), b / samples.size());
    }
}
