package org.alcibiade.composer.video;

/**
 * RGB conversion/wrapper utility.
 */
public class RGB {
    private int rgb;

    public RGB(int rgb) {
        this.rgb = rgb;
    }

    public RGB(int r, int g, int b) {
        r = Math.min(r, 255);
        g = Math.min(g, 255);
        b = Math.min(b, 255);
        this.rgb = r << 16 | g << 8 | b;
    }

    public int getRgbValue() {
        return rgb;
    }

    public int getRed() {
        return (rgb & 0x00FF0000) >> 16;
    }

    public int getGreen() {
        return (rgb & 0x0000FF00) >> 8;
    }

    public int getBlue() {
        return (rgb & 0x000000FF);
    }
}