package org.alcibiade.composer.video;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

public class ImageMixerGenerator implements VideoGenerator {

    private List<VideoGenerator> generators;

    public ImageMixerGenerator(List<VideoGenerator> generators) {
        this.generators = generators;
    }

    public ImageMixerGenerator(VideoGenerator... generators) {
        this(Arrays.asList(generators));
    }

    @Override
    public BufferedImage createImage(FrameParameters frameParameters) {
        BufferedImage resultImage = null;

        for (VideoGenerator generator : generators) {
            BufferedImage localImage = generator.createImage(frameParameters);

            if (resultImage == null) {
                resultImage = localImage;
            } else {
                for (int y = 0; y < localImage.getHeight(); y++) {
                    for (int x = 0; x < localImage.getWidth(); x++) {
                        resultImage.setRGB(x, y, mix(resultImage.getRGB(x, y), localImage.getRGB(x, y)));
                    }
                }
            }
        }

        return resultImage;
    }

    private int mix(int rgbA, int rgbB) {
        RGB a = new RGB(rgbA);
        RGB b = new RGB(rgbB);

        RGB mix = new RGB(
                a.getRed() + b.getRed(),
                a.getGreen() + b.getGreen(),
                a.getBlue() + b.getBlue()
        );

        return mix.getRgbValue();
    }

}

