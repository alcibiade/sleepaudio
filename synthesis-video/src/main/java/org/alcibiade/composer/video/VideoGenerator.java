package org.alcibiade.composer.video;


import java.awt.image.BufferedImage;

public interface VideoGenerator {
    BufferedImage createImage(FrameParameters frameParameters);
}
