package org.alcibiade.composer.video;

public class FrameParameters {

    private int width;
    private int height;
    private double time;

    public FrameParameters(int width, int height, double time) {
        this.width = width;
        this.height = height;
        this.time = time;
    }

    public double getTime() {
        return time;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
