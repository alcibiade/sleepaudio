package org.alcibiade.composer.video;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.awt.image.BufferedImage;

public class BackdropImageGeneratorTest {

    @Test
    public void testSimpleGeneration() {
        BackdropImageGenerator backdropImageGenerator = new BackdropImageGenerator();

        FrameParameters timing = new FrameParameters(640, 480, 0);
        BufferedImage image = backdropImageGenerator.createImage(timing);
        Assertions.assertThat(image).isNotNull();
        Assertions.assertThat(image.getWidth()).isEqualTo(640);
        Assertions.assertThat(image.getHeight()).isEqualTo(480);
        Assertions.assertThat(image.getType()).isEqualTo(BufferedImage.TYPE_INT_RGB);
    }
}
