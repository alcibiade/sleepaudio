package org.alcibiade.composer.engine;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.java.Log;
import org.alcibiade.composer.composer.Composer;
import org.alcibiade.composer.dump.PieceDumpEngine;
import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.render.Renderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * Main composition controller.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
@RequiredArgsConstructor
@Setter
public class CompositionEngine implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(CompositionEngine.class);
    private final List<Composer> composers;
    private final List<Renderer> renderers;
    private String title = "Untitled Piece";
    private Key key = new Key(Pitch.A4, Scale.MINOR);
    private double tempo = 100;
    private int duration;

    @Override
    public void run() {
        logger.info("Starting composition engine");
        logger.debug("Piece title: " + title);
        logger.debug("Piece key:   " + key);

        Piece piece = new Piece(title, key, this.tempo, new Clock(this.duration));

        composers.forEach((composer) -> {
            logger.info("Invoking composer " + composer);
            Track<Event<Note>> composedTrack = composer.composeTrack(piece);
            piece.addTrack(composedTrack);
        });

        PieceDumpEngine dumpEngine = new PieceDumpEngine();
        dumpEngine.dump(piece, 1);

        renderers.forEach((renderer) -> {
            logger.info("Invoking renderer " + renderer);
            renderer.render(piece);
        });
    }
}
