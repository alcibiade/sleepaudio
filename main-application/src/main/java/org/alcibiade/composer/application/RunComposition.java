package org.alcibiade.composer.application;

import org.alcibiade.composer.composer.Bach;
import org.alcibiade.composer.composer.Composer;
import org.alcibiade.composer.engine.CompositionEngine;
import org.alcibiade.composer.piece.Key;
import org.alcibiade.composer.piece.Pitch;
import org.alcibiade.composer.piece.Scale;
import org.alcibiade.composer.random.RandomFactory;
import org.alcibiade.composer.render.CsvMetadataRenderer;
import org.alcibiade.composer.render.InternalAudioRenderer;
import org.alcibiade.composer.render.Renderer;
import org.alcibiade.composer.synthesis.instrument.Piano;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Generate a "Type A" composition.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
@SpringBootApplication
public class RunComposition {

    @Bean
    public RandomFactory getRandomFactory() {
        return new RandomFactory();
    }

    @Bean
    public Piano getPiano() {
        return new Piano();
    }

    @Bean
    public Bach getBach() {
        return new Bach(1);
    }

    @Bean
    public InternalAudioRenderer getAudioRenderer() {
        InternalAudioRenderer audioRenderer = new InternalAudioRenderer();
        audioRenderer.setOutput(new File("composition.flac"));
        return audioRenderer;
    }

    @Bean
    public CsvMetadataRenderer getMetadataRenderer() {
        return CsvMetadataRenderer.builder()
            .output(new File("composition.csv"))
            .fps(25)
            .separator(",")
            .build();
    }


    @Bean
    public CompositionEngine getCompositionEngine(List<Composer> composers, List<Renderer> renderers) {
        CompositionEngine compositionEngine = new CompositionEngine(composers, renderers);
        compositionEngine.setTitle("A Simple Song");
        compositionEngine.setKey(new Key(Pitch.E4, Scale.MINOR));
        compositionEngine.setTempo(120);
        compositionEngine.setDuration(128);
        return compositionEngine;
    }

    /**
     * Run a whole composition process.
     *
     * @param args and optional external context configuration XML file path.
     */
    public static void main(String... args) {
        ApplicationContext globalContext = SpringApplication.run(RunComposition.class, args);
        CompositionEngine engine = globalContext.getBean(CompositionEngine.class);
        engine.run();
    }
}
