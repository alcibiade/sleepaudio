<CsoundSynthesizer>

<CsOptions>
 -g -A -o Sample_Piece.aif
</CsOptions>

<CsVersion>    ; optional section
  After 4.08   ;   Csound version >= 4.09
</CsVersion>

<CsInstruments>
sr = 44100
kr = 4410
ksmps=10
nchnls=2

ga_master_reverb init 0

;
; Instrument 1: Global generators
;

	instr 1
gk_sine_1	oscil 1000, 0.0501, 11
gk_sine_2	oscil 1000, 0.0611, 11

gk_ssine_1  oscil gk_sine_1, 0.0201, 11
gk_ssine_2  oscil gk_sine_2, 0.0211, 11
		endin

;
; Instrument 10: Pad
;

	instr 10
istart	=	p2
idur	=	p3
ifreq	=	p4
iamp	=	p5
ibal	=	p6

asig	oscil   iamp, ifreq, 101
kamp	adsr 1, 0, 1, 3
		outs (1 - ibal) * asig * kamp, (1 + ibal) * asig * kamp
		endin
		
;
; Instrument 11: Voice pad
; Based on Hans Mickelson's voice orchestra file
;

	instr 11
idur	= p3
ifq		= p4
iamp	= p5
ibal	= p6

; Global enveloppe

kenv	adsr 1, 0, 1, 3

; Layer 1 - Grains

k1 		oscil   2, 3 + gk_ssine_2/1000, 112  ; vibrato
k2		linseg  0, idur*.9, 0, idur*.1, 1    ; octaviation coefficient

;                           koct                      iolaps  ifnb
;          xamp  xfund  xform    kband kris  kdur  kdec    ifna    idur
a1		fof kenv/1,ifq+k1,  1030, k2, 200, .003, .017, .005,  10, 112,111, idur, 0, 1
a2		fof kenv/2,ifq+k1,  1370, k2, 200, .003, .017, .005, 20, 112,111, idur, 0, 1
a3		fof kenv/3,ifq+k1,  3170, k2, 200, .003, .017, .005, 20, 112,111, idur, 0, 1
a4		fof kenv/3,ifq+k1,  3797, k2, 200, .003, .017, .005,  30, 112,111, idur, 0, 1
a5		fof kenv/3,ifq+k1,  4177, k2, 200, .003, .017, .005, 30, 112,111, idur, 0, 1
a6		fof kenv/3,ifq+k1,   428, k2, 200, .003, .017, .005, 10, 112,111, idur, 0, 1

aall	= a1 + a2 + a3 + a4 + a5 + a6 

kvoxffq	linseg 0, 5, 3000
avoxlp	lpf18 aall, kvoxffq, 0.1, 0.6

avox = avoxlp

; Layer 2 - Deep base
abaseos	oscil   kenv, ifq, 113
kvbsffq	linseg 0, 5, 3000
abaselp	lpf18 abaseos, kvbsffq, 0.4, 0.2 

abase	= abaselp

; Layers mix

ao		= 0.08 * avox + 0.7 * abase

; Global Filters

apre	= ao

		kfb = 0.5
		adel = 0.01
afl		flanger apre, adel, kfb 
ares	reson afl, 500 + 0.050 * gk_ssine_1, 80
abal	balance ares, afl
alp		lowpass2 abal, 500 + 0.400 * gk_ssine_2, 20
afinal	balance alp, afl

ga_master_reverb = ga_master_reverb + 0.2 * afinal * iamp 	
		outs (1-ibal) * afinal * iamp, (1+ibal) * afinal * iamp

		endin

;
; Instrument 40: Glockenspiel
;

        instr 40
istart  =	p2  
idur    =	p3  
ifreq   =	p4  
iamp    =	p5
ibal	=	p6

asmp    diskin "samples/9272_eliasheuninck_fa_kruis_1.aif.aifc", ifreq/349.228
asig	= asmp * iamp / 30000
adel	delay asig, 0.425
amix	= asig + 0.25 * adel
ga_master_reverb = ga_master_reverb + 0.2 * amix
		outs (1-ibal) * amix, (1+ibal) * amix
		endin
		
;
; Instrument 41: Piano
;

        instr 41
istart  =	p2  
idur    =	p3  
ifreq   =	p4  
iamp    =	p5
ibal	=	p6

asmpl,asmpr diskin "samples/SleepLibrary_Piano.aif", ifreq/220
asigl	= asmpl * iamp / 25000
asigr   = asmpr * iamp / 25000
adel1 	delay asigl, 0.600
adel2 	delay asigl, 1.200
adel3 	delay asigl, 1.800
amixl	= asigl + 0.45 * adel1 + 0.08 * adel2 + 0.04 * adel3
amixr	= asigr + 0.15 * adel1 + 0.18 * adel2 + 0.02 * adel3
ga_master_reverb = ga_master_reverb + 0.13 * amixl + 0.13 * amixr
		outs (1-ibal) * amixl, (1+ibal) * amixr
		endin
		
;
; Intrument 90: Master Reverb
;

	instr 90

arvb    reverb ga_master_reverb, 2.5
ga_master_reverb = 0;
		outs arvb, arvb
		endin

</CsInstruments>

<CsScore>

; Sine
 f11 0 1024 10 1.

f101 0 1024 10 0.8 0.3 0.6
f111 0 1024 19 .5 .5 270 .5
f112 0 4096 10 1
f113 0 1024 10 0.8 0.3 0.6 ; copy of 101
f401 0 1024 10 1.0 0.5 0.25 0.12 0.65 0.3


SCORE
e

</CsScore>

</CsoundSynthesizer>
