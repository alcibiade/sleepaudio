#!/bin/bash

# Expecting to have an audio file context-output.wav and images in a folder context-output-images/

ffmpeg -i context-output-images/frame-%05d.png -i context-output.wav -vf "fps=25,format=yuv420p" -y context-output.mp4
