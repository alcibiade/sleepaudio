package org.alcibiade.composer.synthesis;

import org.alcibiade.composer.application.RunComposition;
import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.render.InternalAudioRenderer;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;

/**
 * Tests for internal renderer.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RunComposition.class)
public class InternalAudioRendererTest {

    private Logger logger = LoggerFactory.getLogger(InternalAudioRendererTest.class);

    @Autowired
    private ApplicationContext context;

    @Test
    public void testRenderPiece() throws IOException {
        Key key = new Key(Pitch.A4, Scale.MINOR);
        Piece piece = new Piece("Sample Piece", key, 90, new Clock(9));

        Note note1 = new Note(Pitch.C3, new Clock(1), Velocity.F);
        Note note2 = new Note(Pitch.G3, new Clock(1), Velocity.F);
        Note note3 = new Note(Pitch.C4, new Clock(1), Velocity.F);
        Note note4 = new Note(Pitch.A4, new Clock(1), Velocity.F);
        Note note5 = new Note(Pitch.C5, new Clock(4), Velocity.F);

        Track<Event<Note>> track = new Track<>(
                "Single note track",
                Instrument.GLOCKENSPIEL);

        track.addEvent(new Event<>(new Clock(0), note1));
        track.addEvent(new Event<>(new Clock(1), note2));
        track.addEvent(new Event<>(new Clock(2), note3));
        track.addEvent(new Event<>(new Clock(3), note4));
        track.addEvent(new Event<>(new Clock(4), note5));

        piece.addTrack(track);

        File outputFile = File.createTempFile("piece_", ".flac");
        outputFile.deleteOnExit();

        InternalAudioRenderer audioRenderer = new InternalAudioRenderer();
        audioRenderer.setApplicationContext(context);
        audioRenderer.setOutput(outputFile);
        audioRenderer.render(piece);

        logger.debug("Piece rendered to {}", outputFile);
        Assertions.assertThat(outputFile.delete()).isTrue();
    }
}
