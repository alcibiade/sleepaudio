package org.alcibiade.composer.synthesis;

import org.alcibiade.composer.application.RunComposition;
import org.alcibiade.composer.synthesis.generator.Generator;
import org.alcibiade.composer.synthesis.instrument.SynthesisInstrument;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

/**
 * Scan and test all instruments.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RunComposition.class)
public class InstrumentsTest {

    private Logger logger = LoggerFactory.getLogger(InstrumentsTest.class);

    @Autowired
    private Set<SynthesisInstrument> instruments;

    @Test
    public void testInstruments() {
        instruments.forEach(instrument -> {
            logger.debug("Testing instrument: {}", instrument);

            Generator note = instrument.playNote(440., 2., 16000);
            Assertions.assertThat(note).isNotNull();

            double maxLevel = 0;

            for (double t = 0; t < 2; t += 0.01) {
                double v = note.getGeneratedValue(t);
                maxLevel = Math.max(maxLevel, Math.abs(v));
            }

            Assertions.assertThat(maxLevel).isGreaterThan(4000);
        });
    }
}
