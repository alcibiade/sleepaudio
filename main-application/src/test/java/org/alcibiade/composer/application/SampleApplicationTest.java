package org.alcibiade.composer.application;

import org.alcibiade.composer.dump.PieceDumpEngine;
import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.render.CSoundRenderer;
import org.alcibiade.composer.render.LilypondRenderer;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class SampleApplicationTest {

    @Test
    public void testSampleApplication() throws IOException {
        Key key = new Key(Pitch.A4, Scale.MINOR);
        Piece piece = new Piece("Sample Piece", key, 90, new Clock(9));

        Note note1 = new Note(Pitch.C3, new Clock(1), Velocity.F);
        Note note2 = new Note(Pitch.G3, new Clock(1), Velocity.F);
        Note note3 = new Note(Pitch.C4, new Clock(1), Velocity.F);
        Note note4 = new Note(Pitch.A4, new Clock(1), Velocity.F);
        Note note5 = new Note(Pitch.C5, new Clock(4), Velocity.F);

        Track<Event<Note>> track = new Track<>(
                "Single note track",
                Instrument.GLOCKENSPIEL);

        track.addEvent(new Event<>(new Clock(0), note1));
        track.addEvent(new Event<>(new Clock(1), note2));
        track.addEvent(new Event<>(new Clock(2), note3));
        track.addEvent(new Event<>(new Clock(3), note4));
        track.addEvent(new Event<>(new Clock(4), note5));

        piece.addTrack(track);

        File lilyFile = File.createTempFile("sample", ".ly");
        File csoundFile = File.createTempFile("sample", ".csd");

        lilyFile.deleteOnExit();
        csoundFile.deleteOnExit();

        CSoundRenderer csdRenderer = new CSoundRenderer();
        csdRenderer.setOutput(csoundFile);
        csdRenderer.render(piece);

        LilypondRenderer lyRenderer = new LilypondRenderer();
        lyRenderer.setOutput(lilyFile);
        lyRenderer.render(piece);

        PieceDumpEngine dumpEngine = new PieceDumpEngine();
        dumpEngine.dump(piece, 4);
    }
}
