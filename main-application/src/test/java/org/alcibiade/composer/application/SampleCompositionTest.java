package org.alcibiade.composer.application;

import org.alcibiade.composer.composer.Bach;
import org.alcibiade.composer.composer.Barbieri;
import org.alcibiade.composer.dump.PieceDumpEngine;
import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.random.RandomFactory;
import org.alcibiade.composer.render.CSoundRenderer;
import org.alcibiade.composer.render.LilypondRenderer;
import org.alcibiade.composer.render.MidiRenderer;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class SampleCompositionTest {

    @Test
    public void testSampleComposition() throws IOException {
        // RandomFactory.setSeed(1974);

        // Create piece
        Key key = new Key(Pitch.A4, Scale.MINOR);
        Piece piece = new Piece("Sample Piece", key, 100, new Clock(64));

        RandomFactory randomFactory = new RandomFactory();

        {
            Bach composer = new Bach(1);
            composer.setRandomFactory(randomFactory);
            piece.addTrack(composer.composeTrack(piece));
        }

        {
            Barbieri composer = new Barbieri(4);
            composer.setRandomFactory(randomFactory);
            piece.addTrack(composer.composeTrack(piece));
        }

        {
            Barbieri composer = new Barbieri(2);
            composer.setRandomFactory(randomFactory);
            piece.addTrack(composer.composeTrack(piece));
        }

        File midiFile = File.createTempFile("sample", ".mid");
        File lilyFile = File.createTempFile("sample", ".ly");
        File csoundFile = File.createTempFile("sample", ".csd");

        midiFile.deleteOnExit();
        lilyFile.deleteOnExit();
        csoundFile.deleteOnExit();

        PieceDumpEngine dumpEngine = new PieceDumpEngine();
        dumpEngine.dump(piece, 4);

        MidiRenderer midiRenderer = new MidiRenderer();
        midiRenderer.setOutput(midiFile);
        midiRenderer.render(piece);

        LilypondRenderer lilyRenderer = new LilypondRenderer();
        lilyRenderer.setOutput(lilyFile);
        lilyRenderer.render(piece);

        CSoundRenderer csdRenderer = new CSoundRenderer();
        csdRenderer.setOutput(csoundFile);
        csdRenderer.render(piece);

        Assertions.assertThat(midiFile).exists();
        Assertions.assertThat(lilyFile).exists();
        Assertions.assertThat(csoundFile).exists();
    }
}
