package org.alcibiade.composer.application;

import org.alcibiade.composer.generator.Generator;
import org.alcibiade.composer.generator.InterpolatingGenerator;
import org.alcibiade.composer.generator.extender.FractalExtender;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class GeneratorTest {
    private static final double TIME_STEP = 0.25;
    private static final int SAMPLES = 64;
    private static final int GRID = 16;
    private Logger logger = LoggerFactory.getLogger(GeneratorTest.class);

    @Test
    public void testGenerator() {
        Random random = new Random();
        Generator gen = new InterpolatingGenerator(0, 80,
                new LinearInterpolator(), new FractalExtender(random, 8, 4, 0.5));
        StringBuilder lines = new StringBuilder();

        for (double t = 0; t <= SAMPLES; t += TIME_STEP) {
            char fillchar = ' ';

            if (t % GRID == 0) {
                fillchar = '-';
            }

            lines.append(displayGeneratorLine(t, gen.get(t), fillchar));
            lines.append("\n");
        }

        logger.debug("Generator {}:\n{}", gen, lines);
    }

    private String displayGeneratorLine(double t, double d, char fillchar) {
        StringBuilder line = new StringBuilder();

        line.append(t);

        while (line.indexOf(".") > line.length() - 3) {
            line.append("0");
        }

        while (line.length() < 8) {
            line.insert(0, ' ');
        }

        line.append('|');

        for (int i = 0; i <= 80; i++) {
            if (i == (int) d) {
                line.append('*');
            } else {
                line.append(fillchar);
            }
        }

        line.append('|');

        return line.toString();
    }
}
