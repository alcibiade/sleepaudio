package org.alcibiade.composer.generator;

import org.alcibiade.composer.generator.extender.ControlPoints;
import org.alcibiade.composer.generator.extender.ControlPointsExtender;
import org.alcibiade.composer.generator.interpolator.Interpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InterpolatingGenerator extends AbstractGenerator {

    private Logger logger = LoggerFactory.getLogger(InterpolatingGenerator.class);
    private Interpolator interpolator;
    private ControlPointsExtender extender;
    private ControlPoints controlPoints = new ControlPoints();

    public InterpolatingGenerator(double low, double high,
                                  Interpolator interpolator, ControlPointsExtender extender) {
        super(low, high);
        this.interpolator = interpolator;
        this.extender = extender;
    }

    @Override
    public double get(double position) {
        if (position >= controlPoints.getDuration() - extender.getExtensionThreshold()) {
            double extendedSize = Math.max(1, position + extender.getExtensionThreshold());
            logger.debug("Position {} is out of current scope, extending control points to {}", position, extendedSize);
            extender.extendControlPoints(controlPoints, extendedSize, getLowBound(), getHighBound());
        }

        double interpolation = interpolator.interpolate(controlPoints, position);

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Interpolation: %7.2f -> %12.6f", position, interpolation));
        }

        return interpolation;
    }
}
