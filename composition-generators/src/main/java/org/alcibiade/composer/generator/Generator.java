package org.alcibiade.composer.generator;

public interface Generator {

    double getLowBound();

    double getHighBound();

    double get(double position);
}
