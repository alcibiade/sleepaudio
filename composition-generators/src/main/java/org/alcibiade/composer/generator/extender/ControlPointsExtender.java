package org.alcibiade.composer.generator.extender;

public interface ControlPointsExtender {

    void extendControlPoints(ControlPoints controlPoints, double targetSize, double low, double high);

    double getExtensionThreshold();
}
