package org.alcibiade.composer.generator;

import java.util.Random;

public class WhiteGenerator extends AbstractGenerator {

    private Random random;

    public WhiteGenerator(Random random) {
        this(random, 0., 1.);
    }

    public WhiteGenerator(Random random, double high) {
        this(random, 0., high);
    }

    public WhiteGenerator(Random random, double low, double high) {
        super(low, high);
        this.random = random;
    }

    @Override
    public double get(double position) {
        return getLowBound() + random.nextDouble()
                * (getHighBound() - getLowBound());
    }
}
