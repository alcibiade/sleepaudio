package org.alcibiade.composer.generator.extender;

import java.util.Random;

public class RandomExtender extends AbstractExtender {

    private double averageInterval;
    private Random random;

    public RandomExtender(Random random, double averageInterval) {
        this.averageInterval = averageInterval;
        this.random = random;
    }

    @Override
    public void extendControlPoints(ControlPoints controlPoints, double targetSize, double low, double high) {
        double effectiveTargetSize = targetSize;

        if (controlPoints.getDuration() < effectiveTargetSize) {
            effectiveTargetSize += 64;
            double pos = controlPoints.getDuration();

            while (pos < effectiveTargetSize) {
                pos += random.nextDouble() * 2. * averageInterval;
                double val = random.nextDouble() * (high - low) + low;
                controlPoints.put(pos, val);
            }

            controlPoints.setDuration(effectiveTargetSize);
        }
    }

    @Override
    public double getExtensionThreshold() {
        return 4 * averageInterval;
    }
}
