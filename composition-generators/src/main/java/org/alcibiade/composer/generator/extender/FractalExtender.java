package org.alcibiade.composer.generator.extender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class FractalExtender extends AbstractExtender {

    private Logger logger = LoggerFactory.getLogger(FractalExtender.class);

    private int initialChunkPoints;
    private double initialChunkSize;
    private double randomFactor;
    private Random random;

    public FractalExtender(Random random,
                           double initialChunkSize,
                           int initialChunkPoints,
                           double randomFactor) {
        this.random = random;
        this.initialChunkSize = initialChunkSize;
        this.randomFactor = randomFactor;
        this.initialChunkPoints = initialChunkPoints;
    }

    @Override
    public void extendControlPoints(ControlPoints controlPoints,
                                    double targetSize, double low, double high) {
        double size = controlPoints.getDuration();

        double effectiveTargetSize = Math.max(targetSize, 2 * size);

        while (size < effectiveTargetSize) {
            if (controlPoints.isEmpty()) {
                for (int cp = 0; cp < initialChunkPoints; cp++) {
                    double t = cp * initialChunkSize / initialChunkPoints;
                    double v = computeRangedValue(low, high);
                    controlPoints.put(t, v);
                }

                size = initialChunkSize;
            } else {
                double randratio = randomFactor * (high - low) / size;
                Map<Double, Double> newpoints = new TreeMap<>();
                for (double t : controlPoints.keySet()) {
                    double v = controlPoints.get(t);
                    double newV = computeNewValue(low, high, v, randratio);

                    newpoints.put(size + t, newV);
                }

                controlPoints.putAll(newpoints);
                size *= 2;
            }

        }

        logger.debug("Extended control points to size {}", size);

        controlPoints.setDuration(size);
    }

    private double computeNewValue(double low, double high, double v, double randratio) {
        double newV = low - 1;
        while (newV < low || newV > high) {
            newV = v + randratio * (random.nextDouble() - 0.5);
        }
        return newV;
    }

    private double computeRangedValue(double low, double high) {
        return random.nextDouble() * (high - low) + low;
    }

    @Override
    public double getExtensionThreshold() {
        return initialChunkSize;
    }
}
