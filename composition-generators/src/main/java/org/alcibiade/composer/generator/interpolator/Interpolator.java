package org.alcibiade.composer.generator.interpolator;

import org.alcibiade.composer.generator.extender.ControlPoints;

public interface Interpolator {

    double interpolate(ControlPoints controlPoints, double position);
}
