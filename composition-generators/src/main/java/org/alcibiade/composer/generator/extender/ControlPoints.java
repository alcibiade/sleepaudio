package org.alcibiade.composer.generator.extender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TreeMap;

/**
 * Control points container
 *
 * @author Yannick Kirschhoffer
 */
public class ControlPoints extends TreeMap<Double, Double> {

    private Logger logger = LoggerFactory.getLogger(ControlPoints.class);

    private double duration = 0;

    @Override
    public Double put(Double key, Double value) {
        duration = Math.max(duration, key);

        if (logger.isTraceEnabled()) {
            logger.trace(String.format("CP[%04d]:%7.2f / %5.2f", size(), key, value));
        }

        return super.put(key, value);
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
