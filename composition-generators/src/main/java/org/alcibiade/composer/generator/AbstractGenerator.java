package org.alcibiade.composer.generator;

public abstract class AbstractGenerator implements Generator {

    private double lowBound;
    private double highBound;

    public AbstractGenerator(double low, double high) {
        lowBound = low;
        highBound = high;
    }

    @Override
    public double getLowBound() {
        return lowBound;
    }

    @Override
    public double getHighBound() {
        return highBound;
    }
}
