package org.alcibiade.composer.generator.interpolator;


import org.alcibiade.composer.generator.extender.ControlPoints;

public class LinearInterpolator extends AbstractInterpolator {

    @Override
    public double interpolate(ControlPoints controlPoints, double position) {
        double previousVal = 0;
        double previousPos = -1;

        double nextVal = 0;
        double nextPos = Double.MAX_VALUE;

        for (Double ctrlPos : controlPoints.keySet()) {
            double ctrlVal = controlPoints.get(ctrlPos);

            if (ctrlPos <= position && ctrlPos > previousPos) {
                previousPos = ctrlPos;
                previousVal = ctrlVal;
            }

            if (ctrlPos > position && ctrlPos < nextPos) {
                nextPos = ctrlPos;
                nextVal = ctrlVal;
            }
        }

        double result = ((position - previousPos) / (nextPos - previousPos))
                * (nextVal - previousVal) + previousVal;

        return result;
    }
}
