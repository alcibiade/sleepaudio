package org.alcibiade.composer.generator;

import org.alcibiade.composer.generator.extender.ControlPoints;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LinearInterpolatorTest {

    private static final double EPSILON = 0.00001;

    @Test
    public void interpolatorTest() {
        LinearInterpolator interpolator = new LinearInterpolator();
        ControlPoints ctl = new ControlPoints();

        // Empty map: zero
        assertEquals(0., interpolator.interpolate(ctl, 0.0), EPSILON);

        // Single value: constant output
        ctl.put(0., 1.5);
        assertEquals(1.5, interpolator.interpolate(ctl, 0.0), EPSILON);
        assertEquals(1.5, interpolator.interpolate(ctl, 0.5), EPSILON);

        // Flat signal: constant output
        ctl.put(1., 1.5);
        assertEquals(1.5, interpolator.interpolate(ctl, 0.0), EPSILON);
        assertEquals(1.5, interpolator.interpolate(ctl, 0.5), EPSILON);
        assertEquals(1.5, interpolator.interpolate(ctl, 1.0), EPSILON);

        // Ramp up
        ctl.put(2., 2.5);
        assertEquals(1.5, interpolator.interpolate(ctl, 1.0), EPSILON);
        assertEquals(2.0, interpolator.interpolate(ctl, 1.5), EPSILON);
        assertEquals(2.5, interpolator.interpolate(ctl, 2.0), EPSILON);

        // Ramp down
        ctl.put(3., 0.);
        assertEquals(2.50, interpolator.interpolate(ctl, 2.0), EPSILON);
        assertEquals(1.25, interpolator.interpolate(ctl, 2.5), EPSILON);
        assertEquals(0.00, interpolator.interpolate(ctl, 3.0), EPSILON);
    }

    /**
     * This will test a bug that affected interpolation between the first control point at position 0 and the
     * next one.
     */
    @Test
    public void interpolatorFirstValuesTest() {
        LinearInterpolator interpolator = new LinearInterpolator();
        ControlPoints ctl = new ControlPoints();
        ctl.put(0., 4.);
        ctl.put(2., 6.);
        assertEquals(4., interpolator.interpolate(ctl, 0.0), EPSILON);
        assertEquals(5., interpolator.interpolate(ctl, 1.0), EPSILON);
        assertEquals(6., interpolator.interpolate(ctl, 2.0), EPSILON);
    }

    /**
     * This will test a bug that affected interpolation at the exact position of loops.
     */
    @Test
    public void interpolateOnKeysTest() {
        LinearInterpolator interpolator = new LinearInterpolator();
        ControlPoints ctl = new ControlPoints();
        ctl.put(0., 1.24);
        ctl.put(1., 9.34);
        ctl.put(2., 6.97);
        ctl.put(3., 1.33);
        ctl.put(4., 1.24);
        ctl.put(5., 9.34);
        ctl.put(6., 6.97);
        ctl.put(7., 1.33);

        Assertions.assertThat(interpolator.interpolate(ctl, 4.)).isEqualTo(1.24);
        Assertions.assertThat(interpolator.interpolate(ctl, 4.1)).isGreaterThan(1.24);
    }
}
