package org.alcibiade.composer.generator;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class WhiteGeneratorTest {

    private static final int TEST_SAMPLES = 10_000;

    @Test
    public void testGeneratorAltConstrucors() {
        Random random = new Random();
        new WhiteGenerator(random);
        new WhiteGenerator(random, 10);
    }

    @Test
    public void testGenerator() {
        Random random = new Random();
        final double low = 1;
        final double high = 10;
        final double delta = (high - low) / 10.;
        Generator gen = new WhiteGenerator(random, low, high);

        int items = 0;
        double total = 0;
        double min = Double.MAX_VALUE;
        double max = 0;

        for (int i = 0; i < TEST_SAMPLES; i++) {
            double value = gen.get(i);
            items += 1;
            total += value;
            min = Math.min(min, value);
            max = Math.max(max, value);
        }

        assertEquals(low, min, delta);
        assertEquals(high, max, delta);
        assertEquals((low + high) / 2., total / items, delta);
    }
}
