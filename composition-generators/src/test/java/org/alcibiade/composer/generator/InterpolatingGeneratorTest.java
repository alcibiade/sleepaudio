package org.alcibiade.composer.generator;

import org.alcibiade.composer.generator.extender.FractalExtender;
import org.alcibiade.composer.generator.extender.RandomExtender;
import org.alcibiade.composer.generator.interpolator.LinearInterpolator;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Random;

public class InterpolatingGeneratorTest {

    @Test
    public void testGenerator() {
        double low = 1;
        double high = 10;
        Random random = new Random();
        LinearInterpolator interpolator = new LinearInterpolator();
        FractalExtender extender = new FractalExtender(random, 4, 4, 2);
        InterpolatingGenerator gen = new InterpolatingGenerator(low, high,
                interpolator, extender);

        for (int t = 0; t < 5; t++) {
            Assertions.assertThat(gen.get((double) t))
                    .isGreaterThanOrEqualTo(low)
                    .isLessThanOrEqualTo(high);
        }
    }

    @Test
    public void testWithoutRandomization() {
        Random random = new Random();
        double low = 1;
        double high = 10;
        LinearInterpolator interpolator = new LinearInterpolator();
        FractalExtender extender = new FractalExtender(random, 4, 4, 0.);
        InterpolatingGenerator gen = new InterpolatingGenerator(low, high,
                interpolator, extender);

        for (int t = 4; t < 24; t++) {
            double currentValue = gen.get((double) t);
            double previousValue = gen.get((double) (t - 4));
            Assertions.assertThat(currentValue)
                    .describedAs("Comparison for t=%d", t)
                    .isEqualTo(previousValue);
        }
    }

    @Test
    public void testRandomExtender() {
        Random random = new Random();
        double low = 0;
        double high = 10;
        LinearInterpolator interpolator = new LinearInterpolator();
        RandomExtender extender = new RandomExtender(random, 10);
        InterpolatingGenerator gen = new InterpolatingGenerator(low, high, interpolator, extender);

        for (int t = 0; t < 5; t++) {
            Assertions.assertThat(gen.get((double) t))
                    .isGreaterThanOrEqualTo(low)
                    .isLessThanOrEqualTo(high);
        }
    }
}
