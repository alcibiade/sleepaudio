package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.*;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Test lilypond output quality.
 */
public class LilypondRendererTest {

    private Logger logger = LoggerFactory.getLogger(LilypondRendererTest.class);

    @Test
    public void testRenderPiece() throws IOException {
        Key key = new Key(Pitch.A4, Scale.MINOR);
        Piece piece = new Piece("Sample Piece", key, 90, new Clock(9));

        Track<Event<Note>> track = new Track<>(
                "Single note track",
                Instrument.GLOCKENSPIEL);

        track.addEvent(new Event<>(new Clock(0), new Note(Pitch.C3, new Clock(1), Velocity.F)));
        track.addEvent(new Event<>(new Clock(1), new Note(Pitch.G3, new Clock(1), Velocity.F)));
        track.addEvent(new Event<>(new Clock(2), new Note(Pitch.C4, new Clock(1), Velocity.F)));
        track.addEvent(new Event<>(new Clock(3), new Note(Pitch.A4, new Clock(1), Velocity.F)));
        track.addEvent(new Event<>(new Clock(4), new Note(Pitch.C5, new Clock(4), Velocity.F)));
        track.addEvent(new Event<>(new Clock(8.0), new Note(Pitch.G2, new Clock(3, 2), Velocity.F)));
        track.addEvent(new Event<>(new Clock(9.5), new Note(Pitch.G2, new Clock(1, 2), Velocity.F)));
        track.addEvent(new Event<>(new Clock(10.), new Note(Pitch.G2, new Clock(4, 2), Velocity.F)));

        piece.addTrack(track);

        File outputFile = File.createTempFile("piece_", ".ly");
        outputFile.deleteOnExit();

        LilypondRenderer audioRenderer = new LilypondRenderer();
        audioRenderer.setOutput(outputFile);
        audioRenderer.render(piece);

        logger.debug("Piece rendered to {}", outputFile);

        String ly = new String(Files.readAllBytes(Paths.get(outputFile.getAbsolutePath())));

        logger.debug("Ly text:\n{}", ly);

        Assertions.assertThat(ly)
                .contains(" \\time 4/4")
                .contains("\\key a \\minor")
                .contains("g,4. g,8 g,2 |");

        Assertions.assertThat(outputFile.delete()).isTrue();
    }
}
