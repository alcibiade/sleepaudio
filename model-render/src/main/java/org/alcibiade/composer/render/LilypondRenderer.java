package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

public class LilypondRenderer implements Renderer {
    private static final Logger logger = LoggerFactory.getLogger(MidiRenderer.class);

    private static final String NOTE_REST = "r";
    private static final String TAGLINE = "Piece generated by SleepAudio Project composer - sleepaudio.sourceforge.net";
    private static final int BAR_DURATION = 4;
    private static final int CENTER_OCTAVE = 3;
    private static final String CLEF_TREBLE = "treble";
    private static final String CLEF_BASS = "bass";
    private File output;

    /**
     * Render the piece in CSound format.
     *
     * @return The CSound source of the piece.
     */
    private static String getAsLilypond(Piece piece) {
        StringBuilder score = new StringBuilder();
        score.append("\\version \"2.11.0\"\n");

        score.append("\\header {\n");
        score.append(String.format("title=\"%s\"\n", piece.getTitle()));
        score.append(String.format("tagline=\"%s\"\n", TAGLINE));
        score.append("}\n");

        score.append("\\score { <<\n");

        char staffIndex = 'A';

        for (Track<Event<Note>> track : piece.getNoteTracks()) {
            boolean isBass = track.getInstrument().getRange().getHigh().compareTo(Pitch.C4) < 0;

            score.append(String.format(" \\context Staff = Staff%s {\n",
                    staffIndex++));
            score.append(String.format("  \\time %d/4\n", BAR_DURATION));
            score.append(String.format("  \\clef %s\n", isBass ? CLEF_BASS : CLEF_TREBLE));
            score.append(String.format("  \\key %s\n", lyKey(piece.getKey())));
            score.append(String.format("  \\set Staff.instrumentName=\"%s\"\n",
                    track.getInstrument().getName()));

            Clock currentPosition = new Clock(0);

            for (Event<Note> noteEvent : track.getEvents()) {
                Event<Note> nextEvent = getNextEvent(track, noteEvent);

                // Add rests to fill potential gap
                if (currentPosition.compareTo(noteEvent.getTime()) < 0) {
                    currentPosition = addNote(score, currentPosition,
                            NOTE_REST, noteEvent.getTime().subtract(currentPosition));
                }

                // Render the note event
                String noteLy = createNoteLy(noteEvent);

                Clock duration = noteEvent.getDuration();

                if (nextEvent != null) {
                    duration = duration.min(nextEvent.getTime().subtract(noteEvent.getTime()));
                }

                currentPosition = addNote(score, currentPosition, noteLy,
                        duration);
            }

            score.append("\n");
            score.append(" }\n");
        }

        score.append(">> }\n");

        return score.toString();
    }

    private static String createNoteLy(Event<Note> noteEvent) {
        String noteLy;
        ChromaticPitch chromaticInformations = noteEvent.getItem().getPitch().
                getChromaticInformations();

        char noteLetter = chromaticInformations.getNoteLetter();
        int octave = chromaticInformations.getOctave();

        StringBuilder noteBuffer = new StringBuilder();
        noteBuffer.append(Character.toLowerCase(noteLetter));
        if (chromaticInformations.isSharp()) {
            noteBuffer.append("is");
        }

        for (int o = octave; o < CENTER_OCTAVE; o++) {
            noteBuffer.append(',');
        }

        for (int o = octave; CENTER_OCTAVE < o; o--) {
            noteBuffer.append('\'');
        }

        noteLy = noteBuffer.toString();
        return noteLy;
    }

    private static Clock addNote(StringBuilder score, Clock position, String note,
                                 Clock duration) {
        Clock currentPosition = position;
        Clock remainingDuration = duration;

        while (remainingDuration.compareTo(Clock.ZERO) > 0) {
            double barPosition = currentPosition.getAsDouble() % BAR_DURATION;
            double barRemain = BAR_DURATION - barPosition;
            boolean dotted = false;

            double myDuration = remainingDuration.getAsDouble();

            if (remainingDuration.getAsDouble() > barRemain) {
                myDuration = barRemain;
            }

            double durationFraction = 4. / myDuration;

            if (durationFraction - Math.floor(durationFraction) > 0.0001) {
                durationFraction = 4. / (myDuration * 2. / 3.);
                dotted = true;
            }
            myDuration = 4. / durationFraction;
            if (dotted) {
                myDuration *= 1.5;
            }

            String noteLy = String.format(" %s%d%s", note, (int) durationFraction, dotted ? "." : "");
            score.append(noteLy);

            logger.debug("Note at {}: duration is {}, fraction is {}, ly is {}",
                    currentPosition,
                    duration,
                    durationFraction,
                    noteLy
            );

            remainingDuration = remainingDuration.subtract(new Clock(myDuration));
            currentPosition = currentPosition.add(new Clock(myDuration));

            if (remainingDuration.compareTo(Clock.ZERO) > 0) {
                score.append(" ~");
            }
        }

        if (currentPosition.mod(BAR_DURATION).equals(Clock.ZERO)) {
            score.append(" |\n");
        }

        return currentPosition;
    }

    private static Event<Note> getNextEvent(Track<Event<Note>> track, Event<Note> lookupEvent) {
        Event<Note> result = null;

        Iterator<Event<Note>> trackEvents = track.getEvents().iterator();

        while (result == null && trackEvents.hasNext()) {
            Event<Note> event = trackEvents.next();
            if (event == lookupEvent && trackEvents.hasNext()) {
                result = trackEvents.next();
            }
        }

        return result;
    }

    private static String lyKey(Key key) {
        String scale = "major";

        if (key.getScale() == Scale.MINOR) {
            scale = "minor";
        }

        char noteLetter = key.getTonic().getChromaticInformations().getNoteLetter();

        return String.format("%s \\%s", Character.toLowerCase(noteLetter),
                scale);
    }

    @Required
    public void setOutput(File output) {
        this.output = output;
    }

    @Override
    public void render(Piece piece) {
        try {
            String score = getAsLilypond(piece);
            Files.write(Paths.get(output.toURI()), score.getBytes());
        } catch (IOException ex) {
            throw new RenderException("Failure while rendering lilypond score", ex);
        }
    }
}
