package org.alcibiade.composer.render;

/**
 * Rendering failure exception wrapper. Renders may mostly fail due to IO exceptions and this can wrap these
 * in runtime exceptions.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class RenderException extends RuntimeException {

    public RenderException(String message, Throwable cause) {
        super(message, cause);
    }
}
