package org.alcibiade.composer.render;

import lombok.Builder;
import lombok.ToString;
import org.alcibiade.composer.piece.*;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Builder
@ToString
public class CsvMetadataRenderer implements Renderer {

    private final File output;
    private final int fps;
    private final String separator;

    @Override
    public void render(Piece piece) {
        double duration_seconds = piece.getDuration().getAsDouble() * 60 / piece.getTempo();
        int framecount = (int) (duration_seconds * this.fps);

        try (FileWriter writer = new FileWriter(this.output)) {
            writer.append(String.format("frame%sclock%skeys\n", separator, separator));

            for (int f = 0; f < framecount; f++) {
                double t = (double) f / fps;
                double beat = t * piece.getTempo() / 60;
                Clock clock = new Clock((int) (beat * 1000), 1000);
                long keys = 0;
                EventSet<Event<Note>> events = piece.getEvents(clock);
                for (Event<Note> event : events) {
                    Pitch pitch = event.getItem().getPitch();
                    double pitch_index = pitch.getPitchIndex();
                    int base_pi = (int) pitch_index;
                    keys |= (1L << base_pi);
                }

                writer.append(String.format("%d%s%f%s%d\n", f, separator, clock.getAsDouble(), separator, keys));
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to render CSV in " + output.getAbsolutePath(), e);
        }
    }
}
