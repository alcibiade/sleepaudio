package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.Piece;

/**
 * Piece rendering interface
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public interface Renderer {

    void render(Piece piece);
}
