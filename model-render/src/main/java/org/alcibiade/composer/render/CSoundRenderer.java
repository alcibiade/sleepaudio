package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.Event;
import org.alcibiade.composer.piece.Note;
import org.alcibiade.composer.piece.Piece;
import org.alcibiade.composer.piece.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CSoundRenderer implements Renderer {

    private static final String SEPARATOR = "; ======================="
            + "=======================================================\n";
    private static final String DECIMAL_SEPARATOR = ".";
    private static final int TAB_STOP = 4;
    private static final int DOUBLE_LEFT_WIDTH = 4;
    private static final int DOUBLE_PRECISION = 6;
    private Logger logger = LoggerFactory.getLogger(MidiRenderer.class);
    private File output;

    /**
     * Render the piece in CSound format.
     *
     * @return The CSound source of the piece.
     */
    private static String getAsCSound(Piece piece) {
        StringBuilder score = new StringBuilder();

        score.append(SEPARATOR);
        score.append("; ===\n");
        score.append("; === ").append(piece.getTitle()).append("\n");
        score.append("; ===\n");
        score.append("; ===     ").append(piece.getKey()).append("\n");
        score.append("; ===\n");
        score.append(SEPARATOR);
        score.append("\n");
        score.append("t 0 ").append(piece.getTempo()).append("\n");
        score.append("\n");
        score.append("i01 0 ").append(piece.getDuration().getAsDouble() + 4).append("\n");
        score.append("i90 0 ").append(piece.getDuration().getAsDouble() + 4).append("\n");
        score.append("\n");

        for (Track<Event<Note>> track : piece.getNoteTracks()) {
            score.append("\n");
            score.append("\n");
            score.append(SEPARATOR);
            score.append("; === Track Name: ").append(track.getName()).append("\n");
            score.append("; === Instrument: ").append(track.getInstrument()).append("\n");
            score.append(SEPARATOR);
            score.append("\n");

            for (Event<Note> event : track.getEvents()) {
                String comment = event.getComment();

                if (comment != null) {
                    for (String line : comment.split("\n")) {
                        score.append("; ");
                        score.append(line);
                        score.append("\n");
                    }
                }

                List<String> items = new ArrayList<>();

                items.add("i" + track.getInstrument().getOrchestraId());
                items.add(formatDouble(event.getTime().getAsDouble()));
                items.add(formatDouble(event.getDuration().getAsDouble()));
                items.add(formatDouble(event.getItem().getPitch().getFrequency()));
                items.add(formatDouble(event.getItem().getVelocity().getDynamic(26_000)));
                items.add(formatDouble(event.getItem().getVelocity().getBalance(2.) - 1));
                items.add("; " + event.getItem().getPitch());

                score.append(formatCSoundEvent(items));
            }
        }

        String template = getCSoundTemplate();
        String csd = template.replaceAll("PIECENAME", piece.getTitle().replace(
                ' ', '_'));
        csd = csd.replaceAll("SCORE", score.toString());

        return csd;
    }

    /**
     * Format a double value as a fixed width string according to fixed format.
     *
     * @param d The value to format
     * @return The formatted string containing the value
     */
    private static String formatDouble(double d) {
        StringBuilder text = new StringBuilder();
        text.append(Double.toString(d));

        int dotoffset = text.indexOf(DECIMAL_SEPARATOR);

        if (dotoffset >= 0) {
            int precision = text.length() - dotoffset - 1;

            while (text.indexOf(DECIMAL_SEPARATOR) < DOUBLE_LEFT_WIDTH) {
                text.insert(0, ' ');
            }

            while (precision < DOUBLE_PRECISION) {
                text.append('0');
                precision++;
            }

            text.setLength(DOUBLE_LEFT_WIDTH + DECIMAL_SEPARATOR.length()
                    + DOUBLE_PRECISION);
        }

        return text.toString();
    }

    /**
     * Render a CSound event as a String
     *
     * @param items A list of all CSound parameters pre-processed as Strings
     * @return The line formatted with consistent tab-stops.
     */
    private static String formatCSoundEvent(List<String> items) {
        StringBuilder line = new StringBuilder();

        for (String item : items) {
            if (line.length() > 0) {
                line.append(' ');
            }

            while (line.length() % TAB_STOP != 0) {
                line.append(' ');
            }

            line.append(item);
        }

        line.append("\n");
        return line.toString();
    }

    /**
     * Read the CSoound piece template from a file inlined in the code repository.
     *
     * @return The CSound template
     */
    protected static String getCSoundTemplate() {
        StringBuilder template = new StringBuilder();
        try {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    CSoundRenderer.class.getResource("model.csd").openStream()))) {
                String tline = reader.readLine();

                while (tline != null) {
                    template.append(tline);
                    template.append('\n');
                    tline = reader.readLine();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return template.toString();
    }

    @Required
    public void setOutput(File output) {
        this.output = output;
    }

    @Override
    public void render(Piece piece) {
        try {
            String csd = getAsCSound(piece);
            logger.info("Writing CSound output to " + output);
            Files.write(Paths.get(output.toURI()), csd.getBytes());
        } catch (IOException ex) {
            throw new RenderException("Failure while rendering CSound data", ex);
        }
    }
}
