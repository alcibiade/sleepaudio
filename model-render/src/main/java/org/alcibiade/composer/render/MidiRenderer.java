package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.*;
import org.alcibiade.composer.piece.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;

/**
 * Render to midi file.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class MidiRenderer implements Renderer {

    private static final int QUARTER_RESOLUTION = 100;
    private Logger logger = LoggerFactory.getLogger(MidiRenderer.class);
    private File output;

    @Required
    public void setOutput(File output) {
        this.output = output;
    }

    @Override
    public void render(Piece piece) {
        try {
            Sequence sequence = getAsMidiSequence(piece);
            logger.info("Writing midi output to " + output);
            MidiSystem.write(sequence, 1, output);
        } catch (IOException ex) {
            throw new RenderException("Failure while rendering midi sequence", ex);
        }
    }

    private Sequence getAsMidiSequence(Piece piece) {
        try {
            int mms = (int) (60 * 1_000_000 / piece.getTempo());
            byte[] tempoData = {(byte) ((mms & 0xFF_0000) >> 16), (byte) ((mms & 0x00_FF00) >> 8), (byte) (mms
                    & 0x00_00FF)};

            Sequence sequence = new Sequence(Sequence.PPQ, QUARTER_RESOLUTION);
            javax.sound.midi.Track masterTrack = sequence.createTrack();
            MetaMessage midiTempoMessage = new MetaMessage();
            midiTempoMessage.setMessage(0x51, tempoData, 3);
            MidiEvent midiTempo = new MidiEvent(midiTempoMessage, 0);
            masterTrack.add(midiTempo);

            int trackIndex = 1;

            for (Track track : piece.getNoteTracks()) {
                logger.debug("Processing track {}", track);
                javax.sound.midi.Track midiTrack = sequence.createTrack();
                ShortMessage instrumentMessage = new ShortMessage();
                instrumentMessage.setMessage(ShortMessage.PROGRAM_CHANGE, trackIndex, track.getInstrument().getGMCode(), 0);
                midiTrack.add(new MidiEvent(instrumentMessage, 0));

                @SuppressWarnings("unchecked") EventSet<Event<Note>> events = track.getEvents();
                for (Event<Note> event : events) {
                    int midiNote = 12 + (int) (event.getItem().getPitch().getPitchIndex());
                    int velocity = (int) event.getItem().getVelocity().getDynamic(127);

                    ShortMessage noteMessageOn = new ShortMessage();
                    noteMessageOn.setMessage(ShortMessage.NOTE_ON, trackIndex, midiNote, velocity);
                    ShortMessage noteMessageOff = new ShortMessage();
                    noteMessageOff.setMessage(ShortMessage.NOTE_OFF, trackIndex, midiNote, velocity);

                    MidiEvent midiEventOn = new MidiEvent(noteMessageOn,
                            (long) (QUARTER_RESOLUTION * event.getTime().getAsDouble()));
                    MidiEvent midiEventOff = new MidiEvent(noteMessageOff,
                            (long) (QUARTER_RESOLUTION * (event.getTime().getAsDouble()
                                    + event.getDuration().getAsDouble())));

                    midiTrack.add(midiEventOn);
                    midiTrack.add(midiEventOff);
                }

                trackIndex++;
            }

            return sequence;
        } catch (InvalidMidiDataException ex) {
            throw new IllegalStateException("Midi sequence generation failed.", ex);
        }
    }
}
