package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.Piece;
import org.alcibiade.composer.video.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class VideoRenderer implements Renderer {

    private Logger logger = LoggerFactory.getLogger(VideoRenderer.class);

    private Path outputFolder;
    private int width = 1280;
    private int height = 720;
    private double framesPerSecond = 25;
    private ExecutorService threadPool;

    @Required
    public void setOutput(Path output) {
        this.outputFolder = output;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setFramesPerSecond(int framesPerSecond) {
        this.framesPerSecond = framesPerSecond;
    }

    @PostConstruct
    public void init() throws IOException {
        if (!Files.exists(this.outputFolder)) {
            Files.createDirectories(outputFolder);
        }

        this.threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public void render(Piece piece) {
        double durationSeconds = 60. * piece.getDuration().getAsDouble() / piece.getTempo();
        int totalFrames = (int) (this.framesPerSecond * durationSeconds);

        VideoGenerator backdrop = new BackdropImageGenerator();
        VideoGenerator pianoRoll = new PianoRollImageGenerator(piece);
        VideoGenerator pianoRollHalo = new HaloGenerator(pianoRoll);

        VideoGenerator mixerGenerator = new ImageMixerGenerator(backdrop, pianoRollHalo, pianoRoll);

        List<Future<Void>> results = new ArrayList<>();

        for (int frame = 0; frame < totalFrames; frame++) {
            FrameRenderer frameRenderer = new FrameRenderer(mixerGenerator, frame, totalFrames);
            results.add(this.threadPool.submit(frameRenderer));
        }

        this.threadPool.shutdown();

        try {
            this.threadPool.awaitTermination(1000, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            throw new IllegalStateException("Interruption while building frames");
        }
    }

    private class FrameRenderer implements Callable<Void> {
        private int frame;
        private VideoGenerator generator;
        private int totalFrames;

        private FrameRenderer(VideoGenerator generator, int frame, int totalFrames) {
            this.frame = frame;
            this.generator = generator;
            this.totalFrames = totalFrames;
        }

        @Override
        public Void call() throws Exception {
            try {
                double time = (double) frame / framesPerSecond;
                logger.info("Rendering frame {} - Time {} - {}%", frame, time, 100 * frame / totalFrames);
                String frameName = String.format("frame-%05d.png", frame);
                Path frameFile = Paths.get(outputFolder.toString(), frameName);

                FrameParameters timing = new FrameParameters(width, height, time);
                BufferedImage image = generator.createImage(timing);

                ImageIO.write(image, "png", frameFile.toFile());
            } catch (IOException ex) {
                throw new IllegalStateException("Could not write frame to file", ex);
            }
            return null;
        }
    }

}
