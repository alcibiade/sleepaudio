package org.alcibiade.composer.render;

import org.alcibiade.composer.piece.Piece;
import org.alcibiade.composer.synthesis.generator.*;
import org.alcibiade.composer.synthesis.instrument.Piano;
import org.alcibiade.composer.synthesis.instrument.SynthesisInstrument;
import org.alcibiade.composer.synthesis.sampling.Sampler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Render as audio using an internal synthesis.
 */
public class InternalAudioRenderer implements Renderer, ApplicationContextAware {

    private Logger logger = LoggerFactory.getLogger(InternalAudioRenderer.class);

    private File output;
    private SynthesisInstrument instrument;

    public void setOutput(File output) {
        this.output = output;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.instrument = applicationContext.getBean(Piano.class);
    }

    @Override
    public void render(Piece piece) {

        Mix wetMix = createGlobalGenerator(piece);

        double pieceDurationSeconds = piece.getDuration().getAsDouble() * 60. / piece.getTempo();
        Sampler audioStreamProvider = new Sampler(wetMix, pieceDurationSeconds);

        try (AudioInputStream ais = audioStreamProvider.getAudioStream()) {
            if (output == null) {
                logger.debug("Setting up line output for sound playback");
                DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioStreamProvider.getAudioFormat());
                SourceDataLine dataLine = (SourceDataLine) AudioSystem.getLine(info);

                dataLine.open(audioStreamProvider.getAudioFormat());

                int bufferSize = dataLine.getBufferSize() / 4;
                byte[] data = new byte[bufferSize];

                dataLine.start();

                while (true) {
                    int read = ais.read(data, 0, data.length);
                    if (read == -1) {
                        break;
                    }

                    long t1 = System.currentTimeMillis();
                    dataLine.write(data, 0, read);
                    long t2 = System.currentTimeMillis();

                    logger.debug("Sending a buffer of {} bytes in {}ms", read, t2 - t1);
                }

                dataLine.stop();
                dataLine.close();
            } else {
                FlacFileWriter waveFileWriter = new FlacFileWriter();
                waveFileWriter.write(ais, output);
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to render audio to " + output, e);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException("Failed to render to audio device", e);
        }
    }

    private Mix createGlobalGenerator(Piece piece) {
        double tempo = piece.getTempo();
        Collection<Generator> inputs = new ArrayList<>();
        piece.getNoteTracks().forEach(track -> track.getEvents().forEach(event -> {
            double pitch = event.getItem().getPitch().getFrequency();
            double duration = event.getDuration().getAsDouble() * 60. / tempo;
            double amplitude = 12000;

            Generator noteGenerator = instrument.playNote(pitch, duration, amplitude);

            Gate gate = Gate.builder().input(noteGenerator).duration(t -> 2 * event.getDuration().getAsDouble() * 60. / tempo).build();
            Delay delay = Delay.builder().input(gate).duration(t -> event.getTime().getAsDouble() * 60. / tempo).build();

            inputs.add(delay);
        }));

        Mix dryMix = Mix.builder().inputs(inputs).build();

        PlateReverb reverb = PlateReverb.builder()
            .input(dryMix)
            .duration(t -> 3.)
            .build();

        reverb.initializeConvolution();

        Amplify reverbAmp = Amplify.builder().input(reverb).envelope(t -> 0.80).build();

        Mix wetMix = Mix.builder().build();
        wetMix.setInputs(dryMix, reverbAmp);

        return dryMix;
    }
}
