package org.alcibiade.composer.render;

import net.sourceforge.javaflacencoder.FLACEncoder;
import net.sourceforge.javaflacencoder.FLACFileOutputStream;
import net.sourceforge.javaflacencoder.StreamConfiguration;

import javax.sound.sampled.AudioInputStream;
import java.io.File;
import java.io.IOException;

public class FlacFileWriter {
    public void write(AudioInputStream ais, File flacFile) throws IOException {

        StreamConfiguration configuration = new StreamConfiguration();
        configuration.setChannelCount(1);
        configuration.setBitsPerSample(16);
        configuration.setSampleRate(44100);

        FLACFileOutputStream fileOutput = new FLACFileOutputStream(flacFile);

        FLACEncoder encoder = new FLACEncoder();
        encoder.setStreamConfiguration(configuration);
        encoder.setOutputStream(fileOutput);
        encoder.openFLACStream();

        while (true) {
            byte[] buffer = new byte[2];
            int l = ais.read(buffer);
            if (l < 2) break;

            int[] ibuffer = new int[2];

            for (int i = 0; i < 2; i++) {
                ibuffer[i] = ((int) (buffer[i]) + 256) % 256;
            }

            int[] samples = {(ibuffer[0] << 8) | ibuffer[1]};


            encoder.addSamples(samples, 1);
            encoder.encodeSamples(1, false);
        }

        encoder.encodeSamples(0, true);
        fileOutput.close();
    }
}
