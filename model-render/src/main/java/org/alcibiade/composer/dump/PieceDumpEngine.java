package org.alcibiade.composer.dump;

import org.alcibiade.asciiart.raster.CharacterRaster;
import org.alcibiade.asciiart.raster.ExtensibleCharacterRaster;
import org.alcibiade.asciiart.raster.RasterContext;
import org.alcibiade.asciiart.widget.TableWidget;
import org.alcibiade.composer.piece.Piece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dump a piece in human readable ascii form.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
public class PieceDumpEngine {

    private Logger logger = LoggerFactory.getLogger(PieceDumpEngine.class);

    public void dump(Piece piece, int divider) {
        TableWidget tableWidget = new TableWidget(new PieceTableModel(piece, divider));

        CharacterRaster raster = new ExtensibleCharacterRaster(' ');
        tableWidget.render(new RasterContext(raster));

        logger.info("Piece:\n" + raster);
    }
}
