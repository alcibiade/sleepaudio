/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.alcibiade.composer.dump;

import org.alcibiade.asciiart.widget.model.AbstractTableModel;
import org.alcibiade.asciiart.widget.model.TableModel;
import org.alcibiade.composer.piece.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Table model to render piece in ascii art table.
 *
 * @author Yannick Kirschhoffer <alcibiade@alcibiade.org>
 */
class PieceTableModel extends AbstractTableModel implements TableModel {

    private Piece piece;
    private int divider;
    private List<Track<Event<Note>>> tracks;

    public PieceTableModel(Piece piece, int divider) {
        this.piece = piece;
        this.divider = divider;
        this.tracks = new ArrayList<>(piece.getNoteTracks());
    }

    @Override
    public int getWidth() {
        return 1 + piece.getNoteTracks().size();
    }

    @Override
    public int getHeight() {
        int height = divider * (int) (piece.getDuration().getAsDouble() - 1);
        return height;
    }

    @Override
    public String getCellContent(int x, int y) {
        String content;

        if (x == 0) {
            Clock clock = new Clock(y, divider);
            content = String.format("%8.2f", clock.getAsDouble());
        } else {
            content = getTrackContent(x - 1, y);
        }

        return content;
    }

    @Override
    public String getColumnTitle(int i) {
        String title;

        if (i == 0) {
            title = "";
        } else {
            title = tracks.get(i - 1).getName();
        }

        return title;
    }

    private String getTrackContent(int x, int y) {
        Clock clockStart = new Clock(y, divider);
        Clock clockEnd = new Clock(y + 1, divider);

        Track<Event<Note>> track = tracks.get(x);
        EventSet<Event<Note>> events = track.getEvents(clockStart, clockEnd);

        StringBuilder text = new StringBuilder();
        for (Event<Note> event : events) {
            Note note = event.getItem();
            Pitch pitch = note.getPitch();
            text.append(pitch);
            text.append(' ');
        }

        return text.toString();
    }
}
