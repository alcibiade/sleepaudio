package org.alcibiade.composer.random;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

/**
 * Test the Random factory.
 */
public class RandomFactoryTest {

    @Test
    public void testSeriesDifferentiation() {
        RandomFactory factory1 = new RandomFactory();
        RandomFactory factory2 = new RandomFactory();

        factory1.setSeed(1337);
        factory2.setSeed(1338);

        Random random1 = factory1.getRandom();
        Random random2 = factory2.getRandom();

        int matches = IntStream.range(0, 100)
                .map(i -> random1.nextInt() == random2.nextInt() ? 1 : 0)
                .sum();

        Assertions.assertThat(matches).isLessThan(100);
    }

    @Test
    public void testSeriesReproduction() {
        RandomFactory factory1 = new RandomFactory();
        RandomFactory factory2 = new RandomFactory();

        factory1.setSeed(1337);
        factory2.setSeed(1337);

        Random random1 = factory1.getRandom();
        Random random2 = factory2.getRandom();

        IntStream.range(0, 100).forEach(i -> Assertions.assertThat(random1.nextInt()).isEqualTo(random2.nextInt()));
    }

    @Test
    public void testSameSeeds() {
        RandomFactory randomFactory = new RandomFactory();

        randomFactory.setSeed(142);
        Random r1 = randomFactory.getRandom();
        randomFactory.setSeed(142);
        Random r2 = randomFactory.getRandom();

        Assertions.assertThat(r1.nextLong()).isEqualTo(r2.nextLong());
        Assertions.assertThat(r1.nextInt()).isEqualTo(r2.nextInt());
        Assertions.assertThat(r1.nextBoolean()).isEqualTo(r2.nextBoolean());
    }

    @Test
    public void testDifferentSeeds() {
        RandomFactory randomFactory = new RandomFactory();

        randomFactory.setSeed(142);
        Random r1 = randomFactory.getRandom();
        randomFactory.setSeed(143);
        Random r2 = randomFactory.getRandom();

        Assertions.assertThat(r1.nextLong()).isNotEqualTo(r2.nextLong());
        assertTrue(r1.nextLong() != r2.nextLong());
    }

    @Test
    public void testOccurrences() {
        RandomFactory randomFactory = new RandomFactory();
        randomFactory.setSeed(142);
        Random r1 = randomFactory.getRandom();
        Random r2 = randomFactory.getRandom();

        assertTrue(r1.nextLong() != r2.nextLong());
    }

    @Test
    public void testDefaultSeed() throws InterruptedException {
        RandomFactory randomFactory = new RandomFactory();
        randomFactory.setSeed(Long.MIN_VALUE);
        Random r1 = randomFactory.getRandom();
        Thread.sleep(100);
        Random r2 = randomFactory.getRandom();

        assertTrue(r1.nextLong() != r2.nextLong());
    }
}
