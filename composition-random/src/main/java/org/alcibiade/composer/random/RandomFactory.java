package org.alcibiade.composer.random;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomFactory {

    private long seed = Long.MIN_VALUE;

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public Random getRandom() {
        Random random;

        if (seed == Long.MIN_VALUE) {
            random = new Random(System.currentTimeMillis());
        } else {
            random = new Random(seed++);
        }

        return random;
    }
}
